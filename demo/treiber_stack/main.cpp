#include "treiber_stack.hpp"

#include <twist/mod/sim.hpp>

// twist::test::WaitGroup
#include <twist/test/wg.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

int main() {
  auto params = twist::sim::pct::Params{};
  params.sim.randomize_malloc = true;
  params.pct.depth = 4;

  auto exp = twist::sim::pct::Explore(params, [] {
    LockFreeStack<int> stack;

    twist::test::WaitGroup{}
        .Add(/*threads=*/4, [&stack] {
          stack.Push(1);
          stack.TryPop();
        })
        .Join();

  }, 10'000);

  fmt::println("Simulations: {}", exp.sims);

  assert(exp.found);

  {
    // Schedule, simulation result
    auto [_, result] = *exp.found;
    assert(result.status == twist::sim::Status::MemoryAccess);
    fmt::println("Stderr: {}", result.std_err);  // Corrupted memory report
  }

  return 0;
}
