#pragma once

#include <twist/ed/std/atomic.hpp>

#include <twist/assist/memory.hpp>

// Treiber lock-free stack

template <typename T>
class LockFreeStack {
  struct Node {
    T data;
    Node* next;
  };

 public:
  void Push(T data) {
    Node* new_top = twist::assist::New<Node>(std::move(data), top_.load());
    while (!top_.compare_exchange_weak(new_top->next, new_top)) {
      // Try again
    }
  }

  std::optional<T> TryPop() {
    while (true) {
      Node* top = top_.load();

      if (top == nullptr) {
        return std::nullopt;
      }

      Node* top_next = twist::assist::Ptr(top)->next;  // <- Memory check

      if (top_.compare_exchange_weak(top, top_next)) {
        auto data = std::move(twist::assist::Ptr(top)->data);  // <- Memory check
        /* ??? */ delete top;  // <- Automatic memory check (Double-free)
        return data;
      }
    }
  }

 private:
  twist::ed::std::atomic<Node*> top_{nullptr};
};
