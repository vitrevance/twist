#include <twist/mod/sim.hpp>

#include <twist/ed/std/mutex.hpp>
#include <twist/ed/std/thread.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

int main() {
  auto params = twist::sim::dfs::Params{};
  params.dfs.max_preempts = 3;

  // Exhaustive search over all interleavings
  auto exp = twist::sim::dfs::Explore(params, [] {
    twist::ed::std::mutex a;
    twist::ed::std::mutex b;

    twist::ed::std::thread t1([&] {
      a.lock();
      b.lock();
      b.unlock();
      a.unlock();
    });

    twist::ed::std::thread t2([&] {
      b.lock();
      a.lock();
      a.unlock();
      b.unlock();
    });

    t1.join();
    t2.join();
  });

  assert(exp.found);

  fmt::println("Simulations: {}", exp.sims);

  {
    // Schedule, simulation result
    auto [_, result] = *exp.found;

    assert(result.status == twist::sim::Status::Deadlock);
    fmt::println("Stderr: {}", result.std_err);  // Deadlock report
  }

  return 0;
}
