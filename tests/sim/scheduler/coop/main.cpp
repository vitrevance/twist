#include <twist/run/sim.hpp>
#include <twist/run/sim/scheduler/coop.hpp>

#include <twist/ed/std/mutex.hpp>
#include <twist/ed/std/thread.hpp>

#include <fmt/core.h>

static_assert(twist::build::IsolatedSim());

int main() {
  {
    // Deadlock

    auto schedule = twist::run::sim::coop::Schedule{42};

    twist::run::sim::coop::Scheduler scheduler{schedule};
    twist::run::sim::Simulator sim{&scheduler};

    auto result = sim.Run([] {
      twist::ed::std::mutex mu1;
      twist::ed::std::mutex mu2;

      twist::ed::std::thread t1([&] {
        mu1.lock();
        twist::ed::std::this_thread::yield();  // <- 2nd switch
        mu2.lock();
      });

      twist::ed::std::thread t2([&] {
        mu2.lock();
        mu1.lock();  // <- 3rd switch
      });

      t1.join();  // <- 1st switch
      t2.join();
    });

    assert(!result.Ok());
    assert(result.preempts == 0);

    fmt::println("Stderr = {}", result.std_err);
  }

  return 0;
}
