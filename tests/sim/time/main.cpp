#include <twist/run/sim.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/thread.hpp>
#include <twist/ed/std/chrono.hpp>

#include <cassert>

using namespace std::chrono_literals;

static_assert(twist::build::Sim());

auto SimSteadyNow() {
  return twist::ed::std::chrono::steady_clock::now();
}

int main() {
  {
    // Time compression

    twist::run::Sim([] {
      static const std::chrono::seconds kPause = 1'000'000s;

      auto start_time = SimSteadyNow();

      twist::ed::std::this_thread::sleep_for(kPause);

      auto end_time = SimSteadyNow();
      assert(end_time >= start_time);

      auto elapsed = end_time - start_time;

      assert(elapsed >= kPause);
    });
  }

  {
    // Timer granularity

    auto params = twist::run::sim::Params{};
    params.sim.tick = 1ms;

    twist::run::Sim(params, [] {
      auto start_time = SimSteadyNow();

      twist::ed::std::this_thread::sleep_for(1us);
      auto end_time = SimSteadyNow();

      auto elapsed = end_time - start_time;
      assert(elapsed >= 1ms);
    });
  }

  {
    // Sync

    auto params = twist::run::sim::Params{};
    params.sim.tick = 1us;

    twist::run::Sim(params, [] {
      twist::ed::std::atomic<int> atom{0};

      auto last = SimSteadyNow();
      for (size_t i = 0; i < 10; ++i) {
        atom.fetch_add(1);

        auto now = SimSteadyNow();
        assert(now > last);
        last = now;
      }
    });
  }

  {
    // Yield

    auto params = twist::run::sim::Params{};
    params.sim.tick = 1us;

    twist::run::Sim(params, [] {
      auto start_time = SimSteadyNow();

      static const size_t kIters = 17;

      for (size_t i = 0; i < kIters; ++i) {
        twist::ed::std::this_thread::yield();
      }

      auto end_time = SimSteadyNow();
      assert(end_time >= start_time);

      auto elapsed = end_time - start_time;
      assert(elapsed == kIters * 1us);
    });
  }

  return 0;
}
