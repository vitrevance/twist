#include <twist/run/sim.hpp>

#include <twist/ed/static/var.hpp>

#include <wheels/core/compiler.hpp>

#include <fmt/core.h>

#include <cassert>
#include <stdexcept>

static_assert(twist::build::IsolatedSim());

struct ThrowInCtor {
  ThrowInCtor() {
    throw 1;
  }
};

TWISTED_STATIC_VAR(ThrowInCtor, g);

int main() {
  auto params = twist::run::sim::Params{};
  params.sim.crash_on_abort = false;

  {
    // Exception thrown from static global variable ctor

    auto result = twist::run::Sim(params, [] {
      WHEELS_UNREACHABLE();  // Aborted _before_ main
    });

    assert(!result.Ok());
    assert(result.status == twist::run::sim::Status::UnhandledException);
    fmt::println("Stderr: {}", result.std_err);
  }

  return 0;
}
