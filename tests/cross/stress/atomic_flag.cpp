#include <twist/run/cross.hpp>

#include <twist/ed/std/atomic.hpp>

#include <twist/test/wg.hpp>
#include <twist/test/plate.hpp>
#include <twist/test/budget.hpp>

class SpinLock {
 public:
  void Lock() {
    while (locked_.test_and_set(std::memory_order::acquire)) {
      while (locked_.test(std::memory_order::relaxed)) {
        ;
      }
    }
  }

  void Unlock() {
    locked_.clear(std::memory_order::release);
  }

 private:
  twist::ed::std::atomic_flag locked_;
};

using namespace std::chrono_literals;

void TestAtomicFlag() {
  twist::run::Cross([] {
    twist::test::TimeBudget budget{3s};
    twist::test::Plate plate;
    SpinLock spinlock;

    twist::test::WaitGroup wg;

    wg.Add(5, [&] {
      for (auto b = budget.MakeClient(); b.Test();) {
        spinlock.Lock();
        plate.Access();
        spinlock.Unlock();
      }
    });

    wg.Join();
  });
}

int main() {
  TestAtomicFlag();
  return 0;
}
