#include <twist/run/cross.hpp>

#include <twist/ed/spin/lock.hpp>

#include <twist/test/wg.hpp>
#include <twist/test/plate.hpp>
#include <twist/test/budget.hpp>

using namespace std::chrono_literals;

void TestSpinLock() {
  twist::run::Cross([] {
    twist::test::TimeBudget budget{5s};
    twist::test::Plate plate;
    twist::ed::SpinLock spinlock;

    twist::test::WaitGroup wg;

    wg.Add(5, [&] {
      for (auto b = budget.MakeClient(); b.Test(); ) {
        std::lock_guard guard(spinlock);
        plate.Access();
      }
    });

    wg.Join();
  });
}

int main() {
  TestSpinLock();
  return 0;
}
