#include <twist/run/cross.hpp>

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/std/thread.hpp>
#include <twist/ed/wait/futex.hpp>
#include <twist/ed/random/number.hpp>

#include <twist/test/repeat.hpp>

#include <fmt/core.h>

#include <chrono>

using namespace std::chrono_literals;

void TestFutexWait() {
  twist::run::Cross([] {
    twist::test::TimeBudget budget{5s};
    twist::test::Repeat repeat{budget};

    while (repeat()) {
      twist::ed::std::atomic<uint32_t> flag{0};

      twist::ed::std::thread t([&] {
        auto wake_key = twist::ed::futex::PrepareWake(flag);
        flag.store(1);
        twist::ed::futex::WakeOne(wake_key);
      });

      twist::ed::futex::Wait(flag, /*old=*/0);

      t.join();
    }

    fmt::println("Iterations: {}", repeat.IterCount());
  });
}

void TestFutexWaitTimed() {
  twist::run::Cross([] {
    twist::test::TimeBudget budget{5s};
    twist::test::Repeat repeat{budget};

    size_t woken_count = 0;

    while (repeat()) {
      twist::ed::std::atomic<uint32_t> flag{0};

      twist::ed::std::thread t([&] {
        auto wake_key = twist::ed::futex::PrepareWake(flag);

        auto delay = std::chrono::milliseconds(twist::ed::random::Number(500, 1500));
        twist::ed::std::this_thread::sleep_for(delay);

        flag.store(1);
        twist::ed::futex::WakeOne(wake_key);
      });

      bool woken = twist::ed::futex::WaitTimed(flag, /*old=*/0, 1s);

      if (woken) {
        ++woken_count;
      }

      t.join();
    }

    fmt::println("Iterations: {}", repeat.IterCount());
    fmt::println("Woken count: {}", woken_count);
  });
}

int main() {
  TestFutexWait();
  TestFutexWaitTimed();
  return 0;
}
