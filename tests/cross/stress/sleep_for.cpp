#include <twist/run/cross.hpp>

#include <twist/ed/std/thread.hpp>
#include <twist/ed/random/number.hpp>

#include <twist/test/repeat.hpp>

#include <fmt/core.h>

#include <chrono>
#include <vector>

using namespace std::chrono_literals;

void TestSleepFor() {
  twist::run::Cross([] {
    twist::test::TimeBudget budget{5s};
    twist::test::Repeat repeat{budget};

    while (repeat()) {
      size_t count = twist::ed::random::Number(1, 10);

      std::vector<twist::ed::std::thread> sleepers;
      sleepers.reserve(count);

      for (size_t i = 0; i < count; ++i) {
        sleepers.emplace_back([] {
          auto delay = 1us * twist::ed::random::Number(1, 1'000'000);
          twist::ed::std::this_thread::sleep_for(delay);
        });
      }

      for (auto& t : sleepers) {
        t.join();
      }
    }

    fmt::println("Iterations: {}", repeat.IterCount());
  });
}

int main() {
  TestSleepFor();
  return 0;
}
