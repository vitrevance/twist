# Twist 🧵

[Fault injection](https://en.wikipedia.org/wiki/Fault_injection) /w [deterministic simulation](https://www.youtube.com/watch?v=4fFDFbi3toc) for modern multi-threaded C++.

## Demo
- [entry] [mutex_lock_order](demo/mutex_lock_order) – simple deadlock with 2 threads and 2 mutexes, exhaustive search with _DFS_ scheduler
- [medium] [bounded_queue](demo/bounded_queue) – bounded blocking queue, _Random_ scheduler
- [expert] [pthread_cond](demo/pthread_cond) – missed wake-up in `pthread_cond_wait`, _PCT_ scheduler
- and [more...](demo)


[Guide (ru)](docs/ru/guide.md)

## 3 (almost) orthogonal dimensions

- Fault injection
- Execution backend: threads / deterministic simulation with fibers
- Sanitizers

![Twist](dims.png)

## Simulation Features

- Determinism
  - Scheduling
  - Randomness
  - Time
  - Memory (in user memory isolation mode)
- Fast context switches
- Customizable schedulers
  - _Fair_ – FIFO + time slices  
  - _Random_ – randomized
      - time slices
      - run queue,
      - wait queues in synchronization primitives / futexes
      - spurious wake-ups, spurious failures in `mutex::try_lock` and `atomic<T>::compare_exchange_weak`
  - _Coop_ – cooperative scheduler for manual simulation
  - _PCT_ – [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)
  - _DFS_ – exhaustive depth-first search
      - TLA<sup>+</sup>-like branching `size_t twist::ed::random::Choose(size_t alts)` and `bool twist::ed::random::Either()`
      - exhaustive search over wake orders in wait queues
      - `max_preempts` bound
      - `max_steps` bound
- Deadlock detection
- Time compression
- Static {global, local} [thread-local] variables and class members simulation
- Compatibility with sanitizers (Address, Thread)
- Simulation digest (including atomic loads)
- User memory isolation
  - Wide class of tolerable failures
  - Automatic determinism checking
  - Randomized memory allocator
  - Memory checks
    - Leaks (automatic),
    - Double-free (automatic),
    - Heap-use-after-free (with `twist::assist::MemoryAccess` or `twist::assist::Ptr<T>`)
  - Detached threads support

### Todo
- [RC11](https://plv.mpi-sws.org/scfix/paper.pdf)
- Dynamic Partial-Order Reduction

## How to use

- `#include <atomic>` → `#include <twist/ed/std/atomic.hpp>`
- `std::atomic<T>` → `twist::ed::std::atomic<T>`

### Twist-ed `SpinLock`

```cpp
#include <twist/ed/std/atomic.hpp>
#include <twist/ed/wait/spin.hpp>

class SpinLock {
 public:
  void Lock() {
    // Execution backend-aware backoff for busy waiting
    twist::ed::SpinWait spin_wait;
    
    while (locked_.exchange(true)) {  // <- Faults injected here
      spin_wait();  // ~ spin_wait.Spin();
    }
  }

  void Unlock() {
    locked_.store(false);  // <- Faults injected here
  }

 private:
  // Drop-in replacement for std::atomic<T>
  twist::ed::std::atomic<bool> locked_{false};
};
```

### Examples

- [futex](/examples/futex/main.cpp) – `twist::ed::futex::Wait`
- [spin](/examples/spin/main.cpp) – `twist::ed::SpinWait`
- [chrono](/examples/chrono/main.cpp) – `twist::ed::std::this_thread::sleep_for` / time compression
- [thread_local](/examples/thread_local/main.cpp) – static thread-local variables
- [static](/examples/static/main.cpp) – static {global, local} variables, static class members
- [debug](/examples/debug/main.cpp) – debugging

### Standard library support

[`twist/ed/std/`](/twist/ed/std), namespace `twist::ed::std::`
- `atomic`, `atomic_flag` (`atomic.hpp`)
- `mutex`, `timed_mutex` (`mutex.hpp`)
- `shared_mutex` (`shared_mutex.hpp`)  
- `condition_variable` (`condition_variable.hpp`)
- `thread` (`thread.hpp`)
- `this_thread::` (`thread.hpp`)
  - `yield`
  - `sleep_for`, `sleep_until`
  - `get_id`
- `random_device` (`random.hpp`)
- `chrono::` (`chrono.hpp`)
  - `system_clock`  
  - `steady_clock`
  - `high_resolution_clock`

### Statics

[`twist/ed/static/`](twist/ed/static) – macros for static variables

- `static T name` → `TWISTED_STATIC_VAR(T, name)`
- `static thread_local T name` → `TWISTED_STATIC_THREAD_LOCAL_VAR(T, name)`
- `static thread_local T* ptr` → `TWISTED_STATIC_THREAD_LOCAL_PTR(T, ptr)`

### Extensions

[`twist/ed/wait/`](twist/ed/wait) – waiting
- Blocking (`futex.hpp`)
  - `Wait` + `WaitTimed` / `PrepareWake` + `Wake{One,All}`
- Spinning (`spin.hpp`)
  - `SpinWait`

[`twist/ed/spin/`](twist/ed/spin) – busy waiting
- `SpinLock` (`lock.hpp`)

[`twist/ed/mutex/`](twist/ed/mutex) – mutual exclusion utilities
- `Locker<Mutex>` (`locker.hpp`)

## CMake Options

- `TWIST_ATOMIC_WAIT` – Support `{atomic, atomic_flag}::wait` (`ON` / `OFF`)

### Runtime

- `TWIST_FAULTY` – Fault injection (`ON` / `OFF`)
  - `TWIST_FAULT_PLACEMENT` – Where to inject faults: `BEFORE` (default) sync operation / `AFTER` / `BOTH` sides
- `TWIST_FIBERS` – Deterministic simulation with fibers (`ON` / `OFF`)
  - `TWIST_FIBERS_ISOLATE_USER_MEMORY` – User memory simulation (`ON` / `OFF`)
    - `TWIST_FIBERS_FIXED_USER_MEMORY` – Use fixed user memory mapping to guarantee deterministic memory addresses (`ON` / `OFF`)

## Build profiles

| Name | CMake options | Description |
| --- | --- | --- |
| _FaultyFibers_ |  `-DTWIST_FAULTY=ON -DTWIST_FIBERS=ON -DTWIST_PRINT_STACKS=ON` | Fault injection + Fibers execution backend |
| _FaultyThreadsASan_ | `-DTWIST_FAULTY=ON -DASAN=ON` | Fault injection + OS threads + Address sanitizer |
| _FaultyThreadsTSan_ | `-DTWIST_FAULTY=ON -DTSAN=ON` | Fault injection + OS threads + Thread sanitizer |

## Dependencies

`binutils-dev` package required for `-DTWIST_PRINT_STACKS=ON`

## Research

- [2016] [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)
- [1996] [Partial-Order Methods for the Verification of Concurrent Systems](https://patricegodefroid.github.io/public_psfiles/thesis.pdf)  
- [2005] [Dynamic Partial-Order Reduction for Model Checking Software](https://users.soe.ucsc.edu/~cormac/papers/popl05.pdf)
- [2008] [Fair Stateless Model Checking](https://www.microsoft.com/en-us/research/publication/fair-stateless-model-checking/)
- [2013] [Bounded Partial-Order Reduction](https://www.microsoft.com/en-us/research/publication/bounded-partial-order-reduction/)
- [2016] [A Practical Approach for Model Checking C/C++11 Code](http://plrg.eecs.uci.edu/publications/toplas16.pdf)
- [2017] [Source Sets: A Foundation for Optimal Dynamic Partial Order Reduction](https://user.it.uu.se/~parosh/publications/papers/jacm17.pdf)  
- [2018] [Effective Stateless Model Checking for C/C++ Concurrency](https://plv.mpi-sws.org/rcmc/paper.pdf)
- [2023] [Truly Stateless, Optimal Dynamic Partial Order Reduction](https://plv.mpi-sws.org/genmc/popl2022-trust.pdf)
