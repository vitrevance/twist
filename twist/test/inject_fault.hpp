#pragma once

namespace twist::test {

// Inject voluntary fault
void InjectFault();

}  // namespace twist::test
