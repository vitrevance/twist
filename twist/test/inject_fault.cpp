#include <twist/test/inject_fault.hpp>

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>

namespace twist::test {

void InjectFault() {
  rt::fiber::user::scheduler::Context context{};
  rt::fiber::user::scheduler::Interrupt(&context);
}

}  // namespace twist::test

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/adversary/inject_fault.hpp>

namespace twist::test {

void InjectFault() {
  rt::thread::fault::InjectFault();
}

}  // namespace twist::test

#else

namespace twist::test {

void InjectFault() {
  // Nop
}

}  // namespace twist::test

#endif
