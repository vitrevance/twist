#pragma once

#include <twist/run/main.hpp>

namespace twist::run {

// Run test routine in test environment
void MultiThread(MainRoutine main);

// Standalone run
void MultiThread(MainRoutine main);

}  // namespace twist::run
