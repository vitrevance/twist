#pragma once

#include <twist/rt/fiber/scheduler/fair/scheduler.hpp>

namespace twist::run::sim {

namespace fair {

using Scheduler = rt::fiber::system::scheduler::fair::Scheduler;

using Params = Scheduler::Params;
using Schedule = Scheduler::Schedule;

}  // namespace fair

}  // namespace twist::run::sim
