#pragma once

#include <twist/rt/fiber/scheduler/coop/scheduler.hpp>

namespace twist::run::sim {

namespace coop {

using Scheduler = rt::fiber::system::scheduler::coop::Scheduler;

using Params = Scheduler::Params;
using Schedule = Scheduler::Schedule;

}  // namespace coop

}  // namespace twist::run::sim
