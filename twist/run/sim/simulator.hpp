#pragma once

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::run::sim {

using Simulator = twist::rt::fiber::system::Simulator;

using SimulatorParams = Simulator::Params;

using Digest = Simulator::Digest;
using Status = Simulator::Status;
using Result = Simulator::Result;

}  // namespace twist::run::sim
