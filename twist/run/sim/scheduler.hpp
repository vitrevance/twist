#pragma once

#include "scheduler/random.hpp"

namespace twist::run::sim {

// Default scheduler
using Scheduler = random::Scheduler;
using Schedule = random::Schedule;

}  // namespace twist::run::sim
