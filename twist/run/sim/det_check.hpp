#pragma once

#include "../main.hpp"
#include "params.hpp"

namespace twist::run::sim {

bool DetCheck(Params, MainRoutine);
bool DetCheck(MainRoutine);

}  // namespace twist::run::sim
