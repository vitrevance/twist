#pragma once

#include <functional>

namespace twist::run {

using MainRoutine = std::function<void()>;

}  // namespace twist::run
