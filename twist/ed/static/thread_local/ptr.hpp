#pragma once

#include <twist/rt/facade/static/thread_local/ptr.hpp>

// static thread_local T* name
// → TWISTED_STATIC_THREAD_LOCAL_PTR(T, name)

// Usage: examples/thread_local/main.cpp
