#pragma once

/*
 * void twist::assist::MemoryAccess(void* addr, size_t size);
 *
 * twist::assist::Ptr<T>
 *
 */

#include <twist/rt/facade/assist/memory.hpp>

namespace twist::assist {

using rt::facade::assist::New;
using rt::facade::assist::MemoryAccess;
using rt::facade::assist::Ptr;

}  // namespace twist::assist
