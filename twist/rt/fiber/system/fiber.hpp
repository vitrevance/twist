#pragma once

#include <twist/rt/fiber/system/fwd.hpp>
#include <twist/rt/fiber/system/fiber/routine.hpp>
#include <twist/rt/fiber/system/fiber/id.hpp>
#include <twist/rt/fiber/system/fiber/at_exit.hpp>
#include <twist/rt/fiber/system/fiber/state.hpp>
#include <twist/rt/fiber/system/futex/wait_queue.hpp>
#include <twist/rt/fiber/system/futex/waiter.hpp>
#include <twist/rt/fiber/system/timer/timer.hpp>
#include <twist/rt/fiber/system/call/status.hpp>
#include <twist/rt/fiber/system/tls.hpp>
#include <twist/rt/fiber/system/trampoline.hpp>
#include <twist/rt/fiber/system/sched_context.hpp>

#include <twist/rt/fiber/system/scheduler/interrupt.hpp>

#include <twist/rt/fiber/system/memory.hpp>

#include <sure/context.hpp>

#include <wheels/intrusive/list.hpp>

#include <optional>
#include <string_view>

namespace twist::rt::fiber {

namespace system {

struct SchedulerTag {};

struct Fiber
    : private sure::ITrampoline,
      public wheels::IntrusiveListNode<Fiber, SchedulerTag> {
 public:
  void Reset(Simulator* _sim, IFiberRoutine* _fun,
        Stack _stack,
        FiberId _id) {

    simulator = _sim;
    routine = _fun;
    stack = std::move(_stack);
    id = _id;

    state = FiberState::Starting;
    main = false;
    at_exit = nullptr;

    futex = nullptr;
    timer = nullptr;
    waiter = nullptr;

    interrupt = nullptr;

    runs = 0;

    context.Setup(stack->MutView(), /*trampoline=*/this);
  }

  // sure::ITrampoline
  void Run() noexcept override {
    Trampoline(this);
  }

  Simulator* simulator;
  IFiberRoutine* routine;  // Object from userspace
  std::optional<Stack> stack;
  sure::ExecutionContext context;
  FiberState state;
  tls::Storage fls;
  FiberId id;
  bool main = false;
  IFiberExitHandler* at_exit{nullptr};

  // For debugging
  const WaiterContext* waiter = nullptr;

  // Futex* system calls
  WaitQueue* futex = nullptr;
  Timer* timer = nullptr;

  const scheduler::InterruptContext* interrupt;

  // For system calls
  call::Status status = call::Status::Ok;

  size_t runs{0};

  // For deadlock report from user-space
  std::string_view report;

  SchedContext sched_context;
};

// Checks

bool NoStackOverflow(Fiber*);

}  // namespace system

}  // namespace twist::rt::fiber
