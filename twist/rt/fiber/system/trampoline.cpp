#include "fiber.hpp"

#include "simulator.hpp"
#include "ss.hpp"

#include <twist/rt/fiber/user/library/fmt/print.hpp>

namespace twist::rt::fiber {

namespace system {

void Trampoline(Fiber* me) {
  Simulator* simulator = me->simulator;

  simulator->SysThreadEnter();

  {
    if (me->main) {
      // Static global variables
      ss::Manager::Instance().ConstructGlobalVars();
    }
  }

  try {
    me->routine->RunUser();
  } catch (...) {
    user::library::fmt::Println(2, "Unhandled exception in Fiber #{}", me->id);
    simulator->SysAbort(Status::UnhandledException);  // Never returns
  }

  {
    if (me->at_exit != nullptr) {
      me->at_exit->AtFiberExit();
    }
  }

  {
    // Cleanup
    if (!me->main) {
      delete me->routine;
    }
  }

  {
    // Static variables

    // Thread-local variables
    tls::Manager::Instance().Destroy(me->fls);

    if (me->main) {
      ss::Manager::Instance().DestroyVars();
    }
  }

  simulator->SysThreadExit();  // Never returns

  WHEELS_UNREACHABLE();
}

}  // namespace system

}  // namespace twist::rt::fiber
