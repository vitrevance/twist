#pragma once

#include <wheels/core/source_location.hpp>
#include <wheels/intrusive/list.hpp>

#include <utility>

namespace twist::rt::fiber {

namespace system {

namespace ss {

//////////////////////////////////////////////////////////////////////

enum class Type {
  Global,
  Local,
};

//////////////////////////////////////////////////////////////////////

struct IVar {
  virtual ~IVar() = default;

  virtual void Ctor() = 0;
  virtual void Dtor() = 0;

  virtual const char* Name() const = 0;
  virtual wheels::SourceLocation Loc() const = 0;

  // Isolated memory
  virtual void Burn() = 0;
};

//////////////////////////////////////////////////////////////////////

struct ListTag {};

struct VarBase : IVar,
                 wheels::IntrusiveListNode<VarBase, ss::ListTag> {
  //
};

//////////////////////////////////////////////////////////////////////

class Manager {
  using VarList = wheels::IntrusiveList<VarBase, ListTag>;

 public:
  Manager() = default;

  ~Manager() {
    globals_.UnlinkAll();
  }

  Type Register(VarBase* var) {
    if (!IsMainRunning()) {
      globals_.PushBack(var);
      return Type::Global;
    } else {
      locals_.PushBack(var);
      var->Ctor();
      return Type::Local;
    }
  }

  // Before main, in user-space
  void ConstructGlobalVars() {
    if (!std::exchange(globals_init_order_, true)) {
      SortGlobals();
    }

    for (auto it = globals_.begin(); it != globals_.end(); ++it) {
      try {
        it->Ctor();
      } catch (...) {
        AbortOnException(*it);
      }
    }
  }

  // After main, in user-space
  void DestroyVars() {
    // Destroy in reverse order

    {
      // Locals
      for (auto it = locals_.rbegin(); it != locals_.rend(); ++it) {
        it->Dtor();
      }
      locals_.UnlinkAll();
    }

    {
      // Globals
      for (auto it = globals_.rbegin(); it != globals_.rend(); ++it) {
        it->Dtor();
      }
    }
  }

  // Isolated memory
  void BurnVars() {
    // Order does not matter

    for (VarBase* var : globals_) {
      var->Burn();
    }

    {
      // Locals
      for (VarBase* var : locals_) {
        var->Burn();
      }
      locals_.UnlinkAll();
    }
  }

  static Manager& Instance() {
    static Manager instance;
    return instance;
  }

 private:
  static bool InitGlobalBefore(wheels::SourceLocation lhs,
                               wheels::SourceLocation rhs) {
    if (lhs.File() != rhs.File()) {
      // Arbitrary
      return lhs.File() < rhs.File();
    }
    // Partial order on variables from the same TU
    return lhs.Line() < rhs.Line();
  }

  void SortGlobals() {
    auto less = [](const IVar* lhs, const IVar* rhs) -> bool {
      return Manager::InitGlobalBefore(lhs->Loc(), rhs->Loc());
    };

    globals_.Sort(less);
  }

  bool IsMainRunning() const;
  void AbortOnException(IVar* global);

 private:
  VarList globals_;
  bool globals_init_order_ = false;

  VarList locals_;
};

}  // namespace ss

}  // namespace system

}  // namespace twist::rt::fiber
