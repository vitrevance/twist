#include <twist/rt/fiber/system/fiber.hpp>

namespace twist::rt::fiber {

namespace system {

// TODO: Annotate user context switches
bool NoStackOverflow(Fiber* fiber) {
  char* sp = (char*)(fiber->context.TryStackPointer());
  WHEELS_UNUSED(sp);
  return true;
}


}  // namespace system

}  // namespace twist::rt::fiber
