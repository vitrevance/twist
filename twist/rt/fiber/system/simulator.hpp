#pragma once

#include <twist/rt/fiber/system/params.hpp>
#include <twist/rt/fiber/system/main.hpp>
#include <twist/rt/fiber/system/loop_budget.hpp>
#include <twist/rt/fiber/system/fiber.hpp>
#include <twist/rt/fiber/system/digest.hpp>
#include <twist/rt/fiber/system/time.hpp>
#include <twist/rt/fiber/system/futex/map.hpp>
#include <twist/rt/fiber/system/futex/waiter.hpp>
#include <twist/rt/fiber/system/futex/wait_queue.hpp>
#include <twist/rt/fiber/system/timer/timer_queue.hpp>
#include <twist/rt/fiber/system/id_allocator.hpp>
#include <twist/rt/fiber/system/result.hpp>
#include <twist/rt/fiber/system/call/handler.hpp>
#include <twist/rt/fiber/system/call/status.hpp>
#include <twist/rt/fiber/system/wake_type.hpp>
#include <twist/rt/fiber/system/memory.hpp>
#include <twist/rt/fiber/system/tls.hpp>
#include <twist/rt/fiber/system/trampoline.hpp>
#include <twist/rt/fiber/system/logger.hpp>
#include <twist/rt/fiber/system/scheduler/scheduler.hpp>
#include <twist/rt/fiber/system/resource_manager.hpp>
#include <twist/rt/fiber/system/status.hpp>

#include <sure/context.hpp>

#include <wheels/core/assert.hpp>

#include <chrono>
#include <map>
#include <optional>
#include <string_view>
#include <sstream>
#include <random>

namespace twist::rt::fiber {

namespace system {

class Simulator
    : private IFiberRoutine,
      private scheduler::ISimContext {
  friend void Trampoline(Fiber*);

  friend struct SleepTimer;
  friend struct FutexTimer;

 public:
  using Params = system::Params;
  using Result = system::Result;
  using Status = system::Status;
  using Digest = uint64_t;

 public:
  Simulator(scheduler::IScheduler* scheduler, Params params = Params(),
            ResourceManager* resource_manager = nullptr);

  scheduler::IScheduler* Scheduler() const  {
    return scheduler_;
  }

  // One-shot
  Result Run(MainRoutine);

  // Debugger mode
  void Start(MainRoutine);
  size_t RunFor(size_t iters);
  bool RunOneIter();
  void Drain();
  Result Burn();

  static Simulator* Current() {
    WHEELS_VERIFY(current, "Not in fiber scheduler");
    return current;
  }

  // TODO: Workaround, remove later
  static Simulator* TryCurrent() {
    return current;
  }

  const Params& GetParams() const {
    return params_;
  }

  void Interrupt(scheduler::InterruptContext*);

  // System calls (Sys{Name})

  // To measure overhead
  void SysNoop();

  Fiber* SysSpawn(IFiberRoutine*);

  void SysYield();

  call::Status SysSleepFor(Time::Duration);
  call::Status SysSleepUntil(Time::Instant);

  // Futex
  call::Status SysFutexWait(FutexKey, const WaiterContext*);
  call::Status SysFutexWaitTimed(FutexKey, Time::Instant, const WaiterContext*);
  void SysFutexWake(FutexKey, size_t);
  void SysFutexFree(FutexKey);  // Cleanup

  /*
   * void Execution::Load(AtomicVar* atomic) {
   *   scheduler_->Pick(atomic.history);
   * }
   *
   * // mutex.Unlock()
   * //   all available
   *
   * // mutex.Unlock()
   * //   -> wake one
   * //
   *
   *
   */

  Time::Instant SysNow() const {
    // Optimize SystemCall: Allocation-free
    return time_.Now();
  }

  uint64_t SysRandomNumber();
  size_t SysRandomChoice(size_t alts);

  void SysLog(wheels::SourceLocation loc, std::string_view message);

  void SysWrite(int fd, std::string_view buf);

  [[noreturn]] void SysAbort(Status);

  // Hints for scheduler
  void SysSchedHintLockFree(bool flag);
  void SysSchedHintProgress();
  void SysSchedHintNewIter();

  void SysThreadEnter();
  [[noreturn]] void SysThreadExit();

  bool AmIUser() const {
    return running_ != nullptr;
  }

  void* UserMalloc(size_t size) {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    return memory_.Malloc(size);
  }

  void UserFree(void* ptr) {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    memory_.Free(ptr);
  }

  void* UserStaticAlloc(size_t size) {
    return memory_.AllocateStatic(size);
  }

  bool UserMemoryAccess(void* addr, size_t size) {
    return memory_.Access(addr, size);
  }

  Fiber* RunningFiber() const {
    WHEELS_ASSERT(AmIUser(), "Not in user-space");
    return running_;
  }

  tls::Manager& TlsManager() {
    return tls_;
  }

  tls::Storage& Tls() {
    return RunningFiber()->fls;
  }

  // false means ignore upcoming Preempt() calls
  void Preemptive(bool on) {
    preemptive_ = on;
  }

  void Silent(bool on) {
    silent_ = on;
  }

  void Debug(bool on) {
    debug_ = on;
  }

  // Statistics for tests
  size_t IterCount() const {
    return iter_;
  }

  size_t InterruptCount() const {
    return interrupt_count_;
  }

  size_t PreemptCount() const {
    return preempt_count_;
  }

  size_t ThreadCount() const {
    return thread_count_;
  }

  // Statistics for tests
  size_t FutexCount() const {
    return futex_wait_count_ + futex_wake_count_;
  }

  void Checkpoint() {
    last_checkpoint_ = iter_;
  }

  void DigestAtomicLoad(uintmax_t repr) {
    if (digest_atomic_loads_) {
      digest_.Load(repr);
    }
  }

 private:
  void ValidateParams();

  void Init();

  void SetCurrentScheduler();
  void ResetCurrentScheduler();

  // IFiberRoutine
  void RunUser() override;

  void SpawnMainFiber(MainRoutine);
  void MainCompleted();

  void StartLoop(MainRoutine);
  // Return number of iterations made
  size_t RunLoop(LoopBudget);
  void StopLoop();

  scheduler::IRunQueue* RunQueue() const;

  void Iter();
  void Maintenance();
  bool Idle();

  void SpuriousWakeups();

  bool IsDeadlock() const;
  void CheckDeadlock();
  void TryPrintDeadlockCycle();
  void ReportFromDeadlockedFiber(Fiber*);
  void ReportDeadlock();

  void CheckMemory();

  void AddTimer(Timer*);
  void CancelTimer(Timer*);
  // true means progress
  bool PollTimers();

  // Advance virtual time
  void Tick();

  // Debugging
  void DebugStart(Fiber*);
  void DebugInterrupt(Fiber*);
  void DebugSwitch(Fiber*);
  void DebugResume(Fiber*);

  // ISimContext

  uint64_t CurrTick() const override {
    return ticker_.Count();
  }

  size_t CurrIter() const override {
    return iter_;
  }

  size_t CurrInterrupt() const override {
    return interrupt_count_;
  }

  // From kernel
  void SchedAbort(std::string error) override {
    stderr_ << "Simulation aborted by scheduler: " << error << std::endl;
    _SysAbort(Status::SchedulerAbort);
  }

  // Context switches

  // Scheduler -> fiber
  void SwitchTo(Fiber*);
  // Running fiber -> scheduler
  call::Status SystemCall(call::Handler handler);

  // Context switch: scheduler -> fiber
  void Run(Fiber* fiber);
  void Iter(Fiber*);
  // true - resume immediately, false - suspend
  bool HandleSystemCall(Fiber*);

  Fiber* CreateFiber(IFiberRoutine* routine);
  void DestroyFiber(Fiber* fiber);

  void Burn(Fiber*);
  void BurnDetachedThreads();

  // Add fiber to run queue
  void ScheduleWaiter(Fiber*, call::Status status = call::Status::Ok);
  void Wake(Fiber* waiter, WakeType);

  // Decision
  bool DigestAtomicLoads() const;
  bool DeterministicMemory() const;

  // System call handlers

  // NOLINTBEGIN
  bool _Interrupt(Fiber* running, const scheduler::InterruptContext*);
  Fiber* _SysSpawn(IFiberRoutine*);
  bool _SysYield(Fiber*);
  void _SysSleep(Fiber*, Timer*);
  void _SysFutexWait(Fiber*, FutexKey, Timer*, const WaiterContext*);
  void _SysFutexWake(Fiber*, FutexKey, size_t);
  void _SysFutexFree(FutexKey);
  void _SysLog(Fiber*, wheels::SourceLocation loc, std::string_view message);
  void _SysWrite(int fd, std::string_view buf);
  void _SysAbort(Status);
  void _SysThreadEnter(Fiber*);
  void _SysThreadExit(Fiber*);
  // NOLINTEND

  //

  size_t AliveCount() const;

 private:
  const Params params_;

  scheduler::IScheduler* scheduler_;

  ResourceManager* resource_manager_;

  Logger logger_;

  Ticker ticker_;
  Time time_;

  Memory memory_;

  tls::Manager tls_;

  IdAllocator ids_;

  DigestCalculator digest_;
  // Cached
  bool digest_atomic_loads_{false};

  std::optional<MainRoutine> main_;

  std::map<FiberId, Fiber*> alive_;

  size_t burnt_threads_ = 0;

  sure::ExecutionContext loop_context_;  // Thread context
  size_t iter_{0};
  // For debugger
  size_t last_checkpoint_{0};

  // Run queue
  // IRunQueue* RunQueue();
  Fiber* running_{nullptr};

  // Wait queues
  FutexMap futex_map_;

  // Timer queue
  TimerQueue timer_queue_;

  call::Handler syscall_;

  bool done_{false};

  std::stringstream stdout_;
  std::stringstream stderr_;
  std::optional<Status> status_;

  // For fault injection
  bool preemptive_{true};
  // For logging
  bool silent_{false};
  bool debug_{false};

  // Statistics
  size_t futex_wait_count_{0};
  size_t futex_wake_count_{0};

  size_t preempt_count_{0};
  size_t interrupt_count_{0};
  size_t thread_count_{0};

  static Simulator* current;
};

}  // namespace system

}  // namespace twist::rt::fiber
