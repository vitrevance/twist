#pragma once

#include <cstdlib>
#include <limits>

namespace twist::rt::fiber {

namespace system {

class LoopBudget {
 public:
  LoopBudget(size_t iters)
      : iters_(iters) {
  }

  static LoopBudget Unlimited() {
    return LoopBudget{std::numeric_limits<size_t>::max()};
  }

  // Prefix: --budget
  bool operator--() {
    return TrySpend(1);
  }

  bool TrySpend(size_t count) {
    if (iters_ >= count) {
      iters_ -= count;
      return true;
    } else {
      return false;
    }
  }

  explicit operator bool() const {
    return iters_ > 0;
  }

 private:
  size_t iters_;
};

}  // namespace system

}  // namespace twist::rt::fiber
