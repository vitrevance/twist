#include "ss.hpp"

#include <twist/rt/fiber/user/library/fmt/format.hpp>
#include <twist/rt/fiber/user/safety/panic.hpp>

#include "simulator.hpp"

namespace twist::rt::fiber {

namespace system {

namespace ss {

bool Manager::IsMainRunning() const {
  return Simulator::TryCurrent() != nullptr;
}

void Manager::AbortOnException(IVar* global) {
  user::Panic(system::Status::UnhandledException, user::library::fmt::FormatToStaticBuf("Exception thrown from constructor of static global variable '{}'", global->Name()), global->Loc());
}

}  // namespace ss

}  // namespace system

}  // namespace twist::rt::fiber
