#pragma once

#include <cstdint>

namespace twist::rt::fiber {

namespace system {

using FutexKey = uint32_t*;

extern const FutexKey kImpossibleFutexKey;

}  // namespace system

}  // namespace twist::rt::fiber
