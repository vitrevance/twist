#pragma once

#include "wait_queue.hpp"
#include "key.hpp"

#include <optional>
#include <vector>

namespace twist::rt::fiber {

namespace system {

// Simple linear probing
// TODO: support rehashing

class FutexHashMap {
  struct Entry {
    std::optional<FutexKey> key;
    WaitQueue queue;
  };

  static const size_t kInitTableSize = 137;

 public:
  FutexHashMap() {
    table_.resize(kInitTableSize);
  }

  WaitQueue* Access(FutexKey key) {
    size_t i = Lookup(key);
    if (!table_[i].key) {
      table_[i].key = key;
    }
    return &table_[i].queue;
  }

  bool IsAllocated(FutexKey key) const {
    size_t i = Lookup(key);
    return table_[i].key == key;
  }

  void Free(FutexKey key) {
    size_t i = Lookup(key);
    if (table_[i].key == key) {
      WHEELS_VERIFY(table_[i].queue.IsEmpty(), "Non-empty wait queue");
      table_[i].key.reset();
    }
  }

  void Clear() {
    for (size_t i = 0; i < table_.size(); ++i) {
      if (table_[i].key) {
        WHEELS_VERIFY(table_[i].queue.IsEmpty(),
                      "Non-empty wait queue");
        table_[i].key.reset();
      }
    }
  }

 private:
  // Or first empty slot
  size_t Lookup(FutexKey key) const {
    size_t i = Hash(key) % table_.size();

    while (table_[i].key) {
      if (table_[i].key == key) {
        return i;
      } else {
        i = (i + 1) % table_.size();
      }
    }

    return i;  // Not found, first empty slot
  }

  static size_t Hash(FutexKey key) {
    return (size_t)((uintptr_t)key >> 4);
  }

 private:
  std::vector<Entry> table_;
};

using FutexMap = FutexHashMap;

}  // namespace system

}  // namespace twist::rt::fiber
