#include "key.hpp"

namespace twist::rt::fiber {

namespace system {

const FutexKey kImpossibleFutexKey = (uint32_t*)1;

}  // namespace system

}  // namespace twist::rt::fiber
