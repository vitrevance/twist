#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace system {

enum FutexWakeCount : size_t {
  All = 0,
  One = 1,
};

}  // namespace system

}  // namespace twist::rt::fiber
