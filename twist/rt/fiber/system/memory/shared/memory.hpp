#pragma once

#include "static.hpp"

#include <twist/rt/fiber/system/params.hpp>

#include <cstdlib>

namespace twist::rt::fiber {

namespace system {

namespace memory::shared {

class Memory {
 public:
  Memory(const Params& /*ignored*/) {
  }

  void Reset() {
    StaticStackAllocator()->Reset();
  }

  void SwitchToFiber() {
    // No-op
  }

  void SwitchToScheduler() {
    // No-op
  }

  // Heap

  void* Malloc(size_t size) {
    return std::malloc(size);
  }

  void Free(void* ptr) {
    std::free(ptr);
  }

  bool Access(void* /*ptr*/, size_t /*size*/) {
    return true;  // No-op
  }

  // Stacks

  void SetMinStackSize(size_t bytes) {
    StaticStackAllocator()->SetMinStackSize(bytes);
  }

  Stack AllocateStack(FiberId hint) {
    return StaticStackAllocator()->Allocate(hint);
  }

  void FreeStack(Stack stack, FiberId hint) {
    StaticStackAllocator()->Release(std::move(stack), hint);
  }

  // Static variables

  void* AllocateStatic(size_t /*size*/) {
    wheels::Panic("Not implemented");
  }

};

}  // namespace memory::shared

}  // namespace system

}  // namespace twist::rt::fiber
