#pragma once

// Implementation
// #include "free_list/list.hpp"
#include "free_list/vector.hpp"

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

namespace heap {

using BlockFreeList = BlockVectorFreeList;

}  // namespace heap

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
