#include "panic.hpp"

#include <wheels/core/panic.hpp>

#include "new.hpp"

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

static void SwitchToSystemAllocatorBeforeAbort() {
  SwitchToSystemHeapAllocator();
}

void HardPanic(std::string_view error, wheels::SourceLocation where) {
  SwitchToSystemAllocatorBeforeAbort();
  wheels::Panic(error, where);
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
