#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

struct IHeapAllocator {
  virtual ~IHeapAllocator() = default;

  virtual void* Malloc(size_t size) = 0;
  virtual void Free(void* ptr) = 0;
};

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
