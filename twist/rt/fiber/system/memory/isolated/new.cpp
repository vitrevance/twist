#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)

#include "new.hpp"

static twist::rt::fiber::system::memory::isolated::IHeapAllocator* allocator = nullptr;

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

void SwitchToSystemHeapAllocator() {
  allocator = nullptr;
}

void SwitchToHeapAllocator(IHeapAllocator* set) {
  allocator = set;
}

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber

//////////////////////////////////////////////////////////////////////

static void* Malloc(std::size_t size) {
  if (allocator != nullptr) {
    // User memory
    return allocator->Malloc(size);
  } else {
    return std::malloc(size);
  }
}

static void Free(void* ptr) {
  if (allocator != nullptr) {
    // User memory
    return allocator->Free(ptr);
  } else {
    std::free(ptr);
  }
}

//////////////////////////////////////////////////////////////////////

// Overload global new / delete

void* operator new(std::size_t s) {
  return Malloc(s);
}

void operator delete(void* p) throw() {
  Free(p);
}

void operator delete(void* p, std::size_t /*s*/) {
  Free(p);
}

// Arrays

void* operator new[](std::size_t s) {
  return Malloc(s);
}

void operator delete[](void* p) throw() {
  Free(p);
}

void operator delete[](void* p, std::size_t /*s*/) throw() {
  Free(p);
}

#endif
