#pragma once

#include "segment_manager.hpp"

#include "statics_allocator.hpp"
#include "heap_allocator.hpp"
#include "heap/allocator.hpp"
#include "stack_allocator.hpp"
#include "new.hpp"

#include <twist/rt/fiber/system/params.hpp>
#include <twist/rt/fiber/system/fiber/id.hpp>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

// Isolated user memory

class Memory : private IHeapAllocator {
 public:
  Memory(const Params& params);

  void Reset();
  void Burn();

  void SwitchToFiber() {
    WHEELS_ASSERT(!user_, "Scheduler mode expected");
    user_ = true;
    SwitchToHeapAllocator(this);
  }

  void SwitchToScheduler() {
    WHEELS_ASSERT(user_, "User mode expected");
    user_ = false;
    SwitchToSystemHeapAllocator();
  }

  // Heap (user)

  // IHeapAllocator
  void* Malloc(size_t size) override;
  void Free(void* ptr) override;

  bool Access(void* ptr, size_t size);

  // Stacks (system)

  void SetMinStackSize(size_t);
  Stack AllocateStack(FiberId /*hint*/);
  void FreeStack(Stack stack, FiberId /*hint*/);

  heap::AllocatorStat HeapStat() const {
    return heap_.Stat();
  }

  // Static variables (user)

  void* AllocateStatic(size_t size);

  //

  bool FixedMapping() const;

 private:
  heap::AllocContext HeapAllocContext();

 private:
  bool randomize_malloc_;

  SegmentManager segments_;

  StaticsAllocator statics_;

  // Heap grows up, stacks - down
  heap::Allocator heap_;
  StackAllocator stacks_;

  bool user_ = false;  // Mode: user / system
};

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
