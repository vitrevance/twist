#pragma once

#include <wheels/system/mmap.hpp>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

class MemoryMapper {
 public:
  MemoryMapper();

  wheels::MutableMemView MutView() {
    return map_.MutView();
  }

  bool FixedAddress() const {
    return fixed_address_;
  }

 private:
  void Mmap();

 private:
  wheels::MmapAllocation map_;
  bool fixed_address_;
};

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
