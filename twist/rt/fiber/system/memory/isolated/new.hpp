#pragma once

#include "heap_allocator.hpp"

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

void SwitchToSystemHeapAllocator();
void SwitchToHeapAllocator(IHeapAllocator*);

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
