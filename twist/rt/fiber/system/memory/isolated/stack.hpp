#pragma once

#include <wheels/memory/view.hpp>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

// Heap-allocated stack without guard page

struct Stack {
  wheels::MutableMemView MutView() {
    return {addr, size};
  }

  char* addr;
  size_t size;
};

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
