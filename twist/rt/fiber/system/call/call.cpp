#include <twist/rt/fiber/user/syscall/abort.hpp>
#include <twist/rt/fiber/user/syscall/yield.hpp>
#include <twist/rt/fiber/user/syscall/sleep.hpp>
#include <twist/rt/fiber/user/syscall/futex.hpp>
#include <twist/rt/fiber/user/syscall/now.hpp>
#include <twist/rt/fiber/user/syscall/random.hpp>
#include <twist/rt/fiber/user/syscall/id.hpp>

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

void Noop() {
  system::Simulator::Current()->SysNoop();
}

void Yield() {
  system::Simulator::Current()->SysYield();
}

void SleepFor(system::Time::Duration delay) {
  system::Simulator::Current()->SysSleepFor(delay);
}

void SleepUntil(system::Time::Instant deadline) {
  system::Simulator::Current()->SysSleepUntil(deadline);
}

system::call::Status FutexWait(system::FutexKey key, system::WaiterContext* ctx) {
  return system::Simulator::Current()->SysFutexWait(key, ctx);
}

system::call::Status FutexWaitTimed(system::FutexKey key, system::Time::Instant d, system::WaiterContext* ctx) {
  return system::Simulator::Current()->SysFutexWaitTimed(key, d, ctx);
}

void FutexWake(system::FutexKey key, size_t count) {
  system::Simulator::Current()->SysFutexWake(key, count);
}

void FutexFree(system::FutexKey key) {
  if (auto* simulator = system::Simulator::TryCurrent()) {
    simulator->SysFutexFree(key);
  }
}

system::FiberId GetId() {
  return system::Simulator::Current()->RunningFiber()->id;
}

system::Time::Instant Now() {
  return system::Simulator::Current()->SysNow();
}

uint64_t RandomNumber() {
  return system::Simulator::Current()->SysRandomNumber();
}

size_t RandomChoice(size_t alts) {
  return system::Simulator::Current()->SysRandomChoice(alts);
}

void Abort(system::Status status) {
  system::Simulator::Current()->SysAbort(status);
}

void Write(int fd, std::string_view buf) {
  system::Simulator::Current()->SysWrite(fd, buf);
}

}  // namespace user::syscall

}  // namespace twist::rt::fiber
