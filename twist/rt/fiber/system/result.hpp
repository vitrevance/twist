#pragma once

#include "status.hpp"

#include <cstdlib>
#include <cstdint>
#include <string>

namespace twist::rt::fiber {

namespace system {

struct Result {
  Status status;

  std::string std_out;
  std::string std_err;

  size_t threads;
  size_t iters;
  size_t interrupts;
  size_t preempts;

  size_t digest;

  bool Ok() const {
    return status == Status::Ok;
  }
};

}  // namespace system

}  // namespace twist::rt::fiber
