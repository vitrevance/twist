#pragma once

namespace twist::rt::fiber {

namespace system {

struct IFiberExitHandler {
  virtual ~IFiberExitHandler() = default;

  virtual void AtFiberExit() noexcept = 0;
};

}  // namespace system

}  // namespace twist::rt::fiber
