#pragma once

#include <cstdint>

namespace twist::rt::fiber {

namespace system {

using FiberId = uint64_t;

extern const FiberId kImpossibleFiberId;

}  // namespace system

}  // namespace twist::rt::fiber
