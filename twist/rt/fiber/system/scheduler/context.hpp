#pragma once

#include <cstdint>
#include <cstdlib>
#include <string>

namespace twist::rt::fiber {

namespace system::scheduler {

struct ISimContext {
  virtual uint64_t CurrTick() const = 0;
  virtual size_t CurrIter() const = 0;
  virtual size_t CurrInterrupt() const = 0;

  virtual void SchedAbort(std::string reason) = 0;
};

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
