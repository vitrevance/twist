#pragma once

namespace twist::rt::fiber {

namespace system::scheduler {

enum class ActionType {
  AtomicLoad,
  AtomicStore,
  AtomicRMW,
  MutexLock,
  MutexTryLock,
  MutexUnlock,
  MutexLockShared,
  MutexTryLockShared,
  MutexUnlockShared,
  CondVarWait,
  CondVarWake,
  ThreadFork,
  ThreadJoin,
  FutexWake,
  FutexWait,
  Local,
  New,
};

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
