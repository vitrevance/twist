#pragma once

#include "action.hpp"

namespace twist::rt::fiber {

namespace system::scheduler {

enum class InterruptPlacement {
  Unknown,
  Before,
  After,
};

struct InterruptContext : Action {
  InterruptPlacement placement = InterruptPlacement::Unknown;
};

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
