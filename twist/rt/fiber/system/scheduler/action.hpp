#pragma once

#include "action_type.hpp"

#include "../fiber/id.hpp"

#include <wheels/core/source_location.hpp>

namespace twist::rt::fiber {

namespace system::scheduler {

struct Action {
  wheels::SourceLocation source_loc;
  const void* var;
  ActionType type;
  const char* name;
  FiberId tid = kImpossibleFiberId;
};

}  // namespace system::scheduler

}  // namespace twist::rt::fiber
