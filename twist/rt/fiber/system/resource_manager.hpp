#pragma once

#include "fiber.hpp"

#include <stack>

namespace twist::rt::fiber {

namespace system {

class ResourceManager {
 public:
  ResourceManager() {
    Warmup();
  }

  Fiber* AllocateFiber() {
    if (!fiber_pool_.empty()) {
      Fiber* top = fiber_pool_.top();
      fiber_pool_.pop();
      return top;
    } else {
      return new Fiber{};
    }
  }

  void ReleaseFiber(Fiber* f) {
    fiber_pool_.push(f);
  }

  ~ResourceManager();

 private:
  void Warmup();

 private:
  std::stack<Fiber*> fiber_pool_;
};

ResourceManager* StaticResourceManager();

}  // namespace system

}  // namespace twist::rt::fiber
