#include "tls.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace system {

namespace tls {

extern const uintptr_t kSlotUninitialized = 1;

Manager& Manager::Instance() {
  return Simulator::Current()->TlsManager();
}

Storage& Manager::Tls() {
  return Simulator::Current()->Tls();
}

}  // namespace tls

}  // namespace system

}  // namespace twist::rt::fiber
