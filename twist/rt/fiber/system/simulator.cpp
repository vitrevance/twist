#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/rt/fiber/system/ss.hpp>
#include <twist/rt/fiber/system/futex/wake_count.hpp>
#include <twist/rt/fiber/system/am_i_user.hpp>

#include <twist/wheels/stacktrace.hpp>

#include <twist/rt/fiber/user/library/fmt/buf_writer.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/panic.hpp>

#include <fmt/core.h>

#include <vector>
#include <utility>
#include <iostream>
#include <set>

namespace twist::rt::fiber {

namespace system {

Simulator* Simulator::current = nullptr;

Simulator::Simulator(scheduler::IScheduler* scheduler, Params params,
                     ResourceManager* resource_manager)
    : params_(params),
      scheduler_(scheduler),
      resource_manager_(resource_manager ? resource_manager : StaticResourceManager()),
      time_(ticker_, params),
      memory_(params) {
  ValidateParams();
}

// Operations invoked by running fibers

void Simulator::SwitchTo(Fiber* fiber) {
  memory_.SwitchToFiber();
  loop_context_.SwitchTo(fiber->context);
  memory_.SwitchToScheduler();

  // WHEELS_ASSERT(NoStackOverflow(fiber), "Fiber stack overflow");
}

call::Status Simulator::SystemCall(call::Handler handler) {
#ifndef NDEBUG
  if (debug_) {
    DebugSwitch(running_);
  }
#endif

  syscall_ = handler;
  running_->context.SwitchTo(loop_context_);
  return running_->status;

#ifndef NDEBUG
  if (debug_) {
    DebugResume(running_);
  }
#endif
}

void Simulator::Interrupt(scheduler::InterruptContext* context) {
#ifndef NDEBUG
  if (debug_) {
    DebugInterrupt(running_);
  }
#endif

  context->tid = running_->id;

  auto interrupt = [this, context](Fiber* me) {
    return _Interrupt(me, context);
  };
  SystemCall(interrupt);
}

// ~ System calls

void Simulator::SysNoop() {
  auto noop = [](Fiber*) {
    return true;  // Resume;
  };

  SystemCall(noop);
}

Fiber* Simulator::SysSpawn(IFiberRoutine* routine) {
  Fiber* fiber;

  auto spawn = [this, routine, &fiber](Fiber* /*me*/) {
    fiber = _SysSpawn(routine);
    return true;  // Continue;
  };

  SystemCall(spawn);

  return fiber;
}

void Simulator::SysYield() {
  auto yield = [this](Fiber* me) {
    return _SysYield(me);
  };

  SystemCall(yield);
}

call::Status Simulator::SysSleepFor(Time::Duration delay) {
  return SysSleepUntil(time_.After(delay));
}

struct SleepTimer final : Timer {
  Fiber* sleeper;

  void Alarm() noexcept override {
    sleeper->simulator->Wake(sleeper, WakeType::SleepTimer);
  }
};

call::Status Simulator::SysSleepUntil(Time::Instant deadline) {
  SleepTimer timer;
  timer.when = deadline;
  timer.sleeper = running_;

  auto sleep = [this, timer = &timer](Fiber* me) -> bool {
    _SysSleep(me, timer);
    return false;  // Suspend
  };

  return SystemCall(sleep);
}

call::Status Simulator::SysFutexWait(FutexKey key, const WaiterContext* ctx) {
  auto wait = [this, key, ctx](Fiber* me) {
    _SysFutexWait(me, key, /*timer=*/nullptr, ctx);
    return false;  // Suspend
  };

  Fiber* me = running_;

  auto status = SystemCall(wait);

  if (WHEELS_UNLIKELY(me->state == FiberState::Deadlocked)) {
    ReportFromDeadlockedFiber(me);
    WHEELS_UNREACHABLE();
  }

  return status;
}

struct FutexTimer : Timer {
  Fiber* waiter = nullptr;

  void Alarm() noexcept override {
    waiter->simulator->Wake(waiter, WakeType::FutexTimer);
  }
};

call::Status Simulator::SysFutexWaitTimed(FutexKey key, Time::Instant d, const WaiterContext* ctx) {
  FutexTimer timer;
  timer.when = d;
  timer.waiter = running_;

  auto wait_timed = [this, key, timer = &timer, ctx](Fiber* me) {
    _SysFutexWait(me, key, timer, ctx);
    return false;  // Suspend
  };

  return SystemCall(wait_timed);
}

void Simulator::SysFutexWake(FutexKey key, size_t count) {
  auto wake = [this, key, count](Fiber* me) {
    _SysFutexWake(/*waker=*/me, key, count);
    return true;  // Continue
  };

  SystemCall(wake);
}

void Simulator::SysFutexFree(FutexKey key) {
  if (!futex_map_.IsAllocated(key)) {
    return;  // Shortcut
  }

  auto free = [this, key](Fiber* /*me*/) {
    _SysFutexFree(key);
    return true;  // Resume
  };

  SystemCall(free);
}

uint64_t Simulator::SysRandomNumber() {
  uint64_t number;

  auto random = [this, number = &number](Fiber* /*me*/) {
    *number = scheduler_->RandomNumber();
    return true;  // Resume
  };

  SystemCall(random);

  return number;
}

size_t Simulator::SysRandomChoice(size_t alts) {
  WHEELS_ASSERT(alts > 0, "alts == 0");

  if (alts == 1) {
    return 0;
  }

  size_t index;

  auto random = [this, alts, index = &index](Fiber* /*me*/) {
    *index = scheduler_->RandomChoice(alts);
    return true;  // Resume
  };

  SystemCall(random);

  return index;
}

void Simulator::SysLog(wheels::SourceLocation loc, std::string_view message) {
  if (silent_) {
    return;
  }

  // NB: wheels::SourceLocation does not own memory, so
  // capturing `loc` by value is ok
  auto log = [this, loc, message](Fiber* me) {
    _SysLog(me, loc, message);
    return true;  // Resume
  };

  SystemCall(log);
}

void Simulator::SysWrite(int fd, std::string_view buf) {
  auto write = [this, fd, buf](Fiber*) {
    _SysWrite(fd, buf);
    return true;  // Resume
  };

  SystemCall(write);
}

void Simulator::SysAbort(Status status) {
  auto abort = [this, status](Fiber*) {
    _SysAbort(status);
    return false;  // Suspend
  };

  SystemCall(abort);  // Never returns

  WHEELS_UNREACHABLE();
}

void Simulator::SysSchedHintLockFree(bool flag) {
  auto hint = [this, flag](Fiber* me) {
    scheduler_->LockFree(me, flag);
    return true;
  };

  SystemCall(hint);
}

void Simulator::SysSchedHintProgress() {
  auto hint = [this](Fiber* me) {
    scheduler_->Progress(me);
    return true;  // Resume
  };

  SystemCall(hint);
}

void Simulator::SysSchedHintNewIter() {
  auto hint = [this](Fiber*) {
    Checkpoint();
    scheduler_->NewIter();
    return true;  // Resume
  };

  SystemCall(hint);
}

void Simulator::SysThreadEnter() {
#ifndef NDEBUG
  if (debug_) {
    DebugStart(running_);
  }
#endif

  auto enter = [this](Fiber* me) {
    _SysThreadEnter(me);
    return true;  // Resume
  };

  SystemCall(enter);
}

void Simulator::SysThreadExit() {
  auto exit = [this](Fiber* me) {
    _SysThreadExit(me);
    return false;  // Suspend
  };

  SystemCall(exit);  // Never returns

  WHEELS_UNREACHABLE();
}

// Scheduling

Result Simulator::Run(MainRoutine main) {
  Init();
  StartLoop(std::move(main));
  RunLoop(LoopBudget::Unlimited());
  StopLoop();

  return Result{
    .status=*status_,
    .std_out=stdout_.str(),
    .std_err=stderr_.str(),
    .threads=ThreadCount(),
    .iters=IterCount(),
    .interrupts=InterruptCount(),
    .preempts=PreemptCount(),
    .digest=digest_.Digest()
  };
}

void Simulator::Start(MainRoutine main) {
  Init();
  StartLoop(std::move(main));
}

size_t Simulator::RunFor(size_t iters) {
  return RunLoop({iters});
}

bool Simulator::RunOneIter() {
  return RunLoop({1}) == 1;
}

void Simulator::Drain() {
  RunLoop(LoopBudget::Unlimited());
  StopLoop();
}

Result Simulator::Burn() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  {
    // Burn fibers
    std::vector<Fiber*> alive;
    for (auto [_, f] : alive_) {
      alive.push_back(f);
    }
    for (Fiber* f : alive) {
      Burn(f);
    }
  }

  {
    // Burn static variables
    ss::Manager::Instance().BurnVars();
  }

  {
    // Reset internal data structures
    timer_queue_.Reset();
    futex_map_.Clear();
    tls_.Reset();
  }

  {
    // Burn memory
    memory_.Burn();
  }

  ResetCurrentScheduler();

  done_ = true;

  return Result{
      .status=*status_,
      .std_out=stdout_.str(),
      .std_err=stderr_.str(),
      .threads=ThreadCount(),
      .iters=IterCount(),
      .interrupts=InterruptCount(),
      .preempts=PreemptCount(),
      .digest=digest_.Digest(),
  };

#else
  WHEELS_PANIC("Scheduler::Burn not supported: set TWIST_FIBERS_ISOLATE_USER_MEMORY=ON");
#endif
}


void Simulator::ValidateParams() {
  // TODO
}

void Simulator::Init() {
  WHEELS_ASSERT(scheduler_ != nullptr, "Scheduler not set");

  memory_.Reset();

  if (params_.min_stack_size) {
    memory_.SetMinStackSize(*params_.min_stack_size);
  }

  time_.Reset();

  // Reset registered thread-locals
  tls_.Reset();

  ids_.Reset();

  digest_.Reset();
  // Cache decision
  digest_atomic_loads_ = DigestAtomicLoads();

  futex_map_.Clear();
  timer_queue_.Reset();  // timer ids

  done_ = false;

  {
    std::stringstream{}.swap(stdout_);
    std::stringstream{}.swap(stderr_);
  }
  status_.reset();

  burnt_threads_ = 0;

  preemptive_ = true;

  iter_ = 0;
  last_checkpoint_ = 0;

  preempt_count_ = 0;

  futex_wait_count_ = 0;
  futex_wake_count_ = 0;
}

// IUserRoutine
void Simulator::RunUser() {
  (*main_)();
}

void Simulator::SpawnMainFiber(MainRoutine main) {
  main_ = std::move(main);

  Fiber* main_fiber = CreateFiber(/*routine=*/this);
  main_fiber->at_exit = nullptr;
  main_fiber->main = true;

  scheduler_->Spawn(main_fiber);
}

void Simulator::BurnDetachedThreads() {
  std::vector<Fiber*> detached;

  for (auto [_, f] : alive_) {
    if (!f->main && (f->at_exit == nullptr)) {
      detached.push_back(f);
    }
  }

  for (Fiber* f : detached) {
    Burn(f);
  }
}

void Simulator::MainCompleted() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  if (params_.allow_detached_threads) {
    BurnDetachedThreads();
  }
#endif

  WHEELS_VERIFY(alive_.empty(), "There are alive fibers after main completion");

  CheckMemory();

  status_ = Status::Ok;
}

void Simulator::StartLoop(MainRoutine main) {
  SetCurrentScheduler();
  scheduler_->Start(this);
  SpawnMainFiber(std::move(main));
}

void Simulator::SetCurrentScheduler() {
  WHEELS_VERIFY(current == nullptr, "Cannot run simulator from another simulator");
  current = this;
}

void Simulator::ResetCurrentScheduler() {
  current = nullptr;
}

scheduler::IRunQueue* Simulator::RunQueue() const {
  return scheduler_;
}

void Simulator::Tick() {
  ticker_.Tick();
}

void Simulator::Maintenance() {
  if (!timer_queue_.IsEmpty()) {
    PollTimers();
  }
#if defined(__TWIST_FAULTY__)
  if (!IsDeadlock()) {
    SpuriousWakeups();
  }
#endif
}

// Precondition: run queue is empty
// true - keep running, false - stop run loop
bool Simulator::Idle() {
  CheckDeadlock();

  if (timer_queue_.IsEmpty()) {
    return false;  // Stop
  }

  if (PollTimers()) {
    return true;  // Keep running
  }

  time_.AdvanceTo(timer_queue_.NextDeadLine());
  PollTimers();
  return true;
}

void Simulator::Run(Fiber* fiber) {
  fiber->state = FiberState::Running;
  ++fiber->runs;
  running_ = fiber;
  SwitchTo(fiber);
  running_ = nullptr;
}

void Simulator::Iter(Fiber* f) {
  f->interrupt = nullptr;

  digest_.Iter(f);

  do {
    Run(f);
  } while (HandleSystemCall(f));

  WHEELS_VERIFY(preemptive_, "Fiber yields control in non-preemptive mode");
}

void Simulator::Iter() {
  ++iter_;

  if (scheduler_->Maintenance()) {
    Maintenance();
  }

  {
    Fiber* next = RunQueue()->Next();
    Iter(next);
  }
}

struct SysAbortEx {
};

size_t Simulator::RunLoop(LoopBudget iters) {
  if (done_) {
    return 0;
  }

  size_t start_iter = iter_;

  try {
    do {
      while (!RunQueue()->IsIdle() && --iters) {
        Iter();
      }
    } while (iters && Idle());
  } catch (SysAbortEx) {
    Burn();
  }

  return (iter_ - start_iter);
}

bool Simulator::PollTimers() {
  bool progress = false;

  auto now = time_.Now();
  while (Timer* timer = timer_queue_.Poll(now)) {
    progress = true;
    timer->Alarm();
  }

  return progress;
}

void Simulator::AddTimer(Timer* timer) {
  timer_queue_.Add(timer);
}

void Simulator::CancelTimer(Timer* timer) {
  WHEELS_VERIFY(timer_queue_.Remove(timer), "Timer not found");
}

bool Simulator::HandleSystemCall(Fiber* caller) {
  if (bool resume = syscall_(caller); resume) {
    caller->status = call::Status::Ok;
    return true;  // Resume caller
  } else {
    return false;  // Suspend caller
  }
}

// True - resume, false - preempt
bool Simulator::_Interrupt(Fiber* running, const scheduler::InterruptContext* context) {
  ++interrupt_count_;

  running->interrupt = context;

  Tick();

  if (preemptive_ && scheduler_->Preempt(running, context)) {
    ++preempt_count_;
    return false;  // Preempted -> Suspend
  } else {
    return true;  // Resume
  }
}

Fiber* Simulator::_SysSpawn(IFiberRoutine* routine) {
  Fiber* fiber = CreateFiber(routine);
  scheduler_->Spawn(fiber);
  return fiber;
}

bool Simulator::_SysYield(Fiber* fiber) {
  Tick();

  scheduler_->Yield(fiber);
  return false;  // Always reschedule
}

void Simulator::_SysSleep(Fiber* sleeper, Timer* timer) {
  AddTimer(timer);

  {
    sleeper->state = FiberState::Sleeping;

    sleeper->futex = nullptr;
    sleeper->timer = timer;
    sleeper->waiter = nullptr;
  }
}

static bool VulnerableToSpuriousWakeup(const WaiterContext* waiter) {
  switch (waiter->type) {
    case FutexType::Futex:
    case FutexType::Atomic:
    case FutexType::CondVar:
    case FutexType::MutexTryLock:
      return true;
    default:
      return false;
  }
}

void Simulator::_SysFutexWait(Fiber* waiter, FutexKey key, Timer* timer, const WaiterContext* ctx) {
  if (VulnerableToSpuriousWakeup(ctx)) {
    if (scheduler_->SpuriousWakeup()) {
      ScheduleWaiter(waiter, call::Status::Interrupted);
      return;
    }
  }

  WaitQueue* wq = futex_map_.Access(key);
  wq->Push(waiter);

  if (timer != nullptr) {
    AddTimer(timer);
  }

  {
    waiter->state = FiberState::Parked;

    waiter->futex = wq;
    waiter->timer = timer;
    waiter->waiter = ctx;
  }

  // Statistics
  ++futex_wait_count_;
}

void Simulator::_SysFutexWake(Fiber* /*waker*/, FutexKey key, size_t count) {
  WaitQueue* wq = futex_map_.Access(key);

  if (count == 1) {
    // One
    if (Fiber* waiter = wq->Pop(); waiter != nullptr) {
      Wake(waiter, WakeType::FutexWake);
    }
  } else if (count == FutexWakeCount::All) {
    // All
    while (Fiber* waiter = wq->PopAll()) {
      Wake(waiter, WakeType::FutexWake);
    }
  } else {
    WHEELS_PANIC("'count' value " << count << " not supported");
  }

  // Statistics
  ++futex_wake_count_;
}

void Simulator::_SysFutexFree(FutexKey key) {
  futex_map_.Free(key);
}

void Simulator::_SysLog(Fiber* logger, wheels::SourceLocation loc, std::string_view message) {
  LogEvent event{
    .source_loc=loc,
    .id=logger->id,
    .message=std::string{message},
  };

  logger_.Log(std::move(event));
};

void Simulator::_SysWrite(int fd, std::string_view buf) {
  if (fd == 1) {
    stdout_.write(buf.data(), buf.size());
    if (params_.forward_stdout && !silent_) {
      std::cout.write(buf.data(), buf.size());
    }
  } else if (fd == 2) {
    stderr_.write(buf.data(), buf.size());
  } else {
    // TODO: Abort simulation
    WHEELS_PANIC("Cannot write to fd " << fd);
  }
}

void Simulator::_SysAbort(Status status) {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  status_ = status;
  throw SysAbortEx{};
#else
  WHEELS_UNUSED(status);  // TODO
  wheels::Panic(stderr_.str());
#endif
}

void Simulator::_SysThreadEnter(Fiber* /*fiber*/) {
  // No-op
}

void Simulator::_SysThreadExit(Fiber* fiber) {
  scheduler_->Exit(fiber);

  bool main = fiber->main;

  DestroyFiber(fiber);

  if (main) {
    MainCompleted();
  }
}

void Simulator::ScheduleWaiter(Fiber* waiter, call::Status status) {
  waiter->status = status;
  scheduler_->Wake(waiter);
}

// From simulator context
void Simulator::Wake(Fiber* waiter, WakeType wake) {
  WHEELS_ASSERT(waiter->state == FiberState::Parked || waiter->state == FiberState::Sleeping,
                "Unexpected fiber state");

  call::Status status;

  switch (wake) {
    case WakeType::SleepTimer:
      // SleepUntil -> Timeout
      status = call::Status::Ok;
      break;

    case WakeType::FutexWake:
      status = call::Status::Ok;
      if (waiter->timer != nullptr) {
        // wq->ParkTimed -> wq->Wake
        CancelTimer(waiter->timer);
      } else {
        // wq->Park -> wq->Wake
        // Do nothing
      }
      break;

    case WakeType::FutexTimer:
      // wq->ParkTimed -> Timeout
      status = call::Status::Timeout;
      waiter->futex->Remove(waiter);
      break;

    case WakeType::Spurious:
      // Spurious wake-up
      status = call::Status::Interrupted;
      if (waiter->futex != nullptr) {
        waiter->futex->Remove(waiter);
      }
      if (waiter->timer != nullptr) {
        CancelTimer(waiter->timer);
      }

      break;
  }

  {
    // Reset fiber state

    waiter->state = FiberState::Runnable;

    waiter->futex = nullptr;
    waiter->timer = nullptr;
    waiter->waiter = nullptr;
  }

  ScheduleWaiter(waiter, status);
}

Fiber* Simulator::CreateFiber(IFiberRoutine* routine) {
  ++thread_count_;

  auto id = ids_.AllocateId();
  auto stack = memory_.AllocateStack(/*hint=*/id);

  // Kernel-space allocation
  Fiber* fiber = resource_manager_->AllocateFiber();
  fiber->Reset(this, routine, std::move(stack), id);

  alive_.emplace(fiber->id, fiber);

  return fiber;
}

void Simulator::DestroyFiber(Fiber* fiber) {
  alive_.erase(fiber->id);

  {
    memory_.FreeStack(std::move(*(fiber->stack)), /*hint=*/fiber->id);
    fiber->stack.reset();
  }
  ids_.Free(fiber->id);

  resource_manager_->ReleaseFiber(fiber);
}

void Simulator::Burn(Fiber* fiber) {
  {
    RunQueue()->Remove(fiber);
    if (fiber->timer) {
      timer_queue_.Remove(fiber->timer);
    }
    if (fiber->futex) {
      fiber->futex->Remove(fiber);
    }
  }

  ++burnt_threads_;

  DestroyFiber(fiber);
}

// O(1)
size_t Simulator::AliveCount() const {
  return alive_.size();
}

void Simulator::StopLoop() {
  WHEELS_VERIFY(RunQueue()->IsIdle(), "Broken scheduler");
  WHEELS_VERIFY(timer_queue_.IsEmpty(), "Broken scheduler");

  done_ = true;

  main_.reset();

  ResetCurrentScheduler();
}

void Simulator::CheckMemory() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  if (burnt_threads_ > 0) {
    return;  // Incompatible with memory leaks check
  }

  auto stat = memory_.HeapStat();

  if (stat.alloc_count != stat.free_count) {
    size_t bytes_leaked = stat.user_bytes_allocated - stat.user_bytes_freed;
    size_t leak_count = stat.alloc_count - stat.free_count;

    stderr_ << "Memory leaks: "
        << bytes_leaked << " bytes leaked" << " (" << leak_count << " allocation(s))";

    _SysAbort(Status::MemoryLeak);
  }
#endif
}

bool Simulator::IsDeadlock() const {
  return RunQueue()->IsIdle() &&
         timer_queue_.IsEmpty() &&
         AliveCount() > 0;
}

void Simulator::CheckDeadlock() {
  if (IsDeadlock()) {
    // Validate
    for (auto [_, f] : alive_) {
      WHEELS_VERIFY(f->state == FiberState::Parked, "Internal error");
    }
    ReportDeadlock();
    _SysAbort(Status::Deadlock);
  }
}

void Simulator::ReportDeadlock() {
  stderr_ << "Deadlock detected in fiber scheduler: "
          << alive_.size() << " fibers blocked\n";

  stderr_ << fmt::format("Scheduler loop iterations: {}\n", iter_);
  if (last_checkpoint_ != 0) {
    stderr_ << fmt::format("Last checkpoint: iteration #{}\n", last_checkpoint_);
  }

  stderr_ << std::endl;  // Blank line

  TryPrintDeadlockCycle();

  for (auto [_, fiber] : alive_) {
    fiber->state = FiberState::Deadlocked;
    running_ = fiber;
    SwitchTo(fiber);
    std::string_view report = fiber->report;
    stderr_.write(report.begin(), report.size());
  }
}

void Simulator::TryPrintDeadlockCycle() {
  Fiber* start = nullptr;

  {
    // Any
    Fiber* curr = alive_.begin()->second;

    std::set<Fiber*> path;

    while (curr->waiter->waiting_for) {
      path.insert(curr);
      Fiber* next = alive_[*(curr->waiter->waiting_for)];
      if (path.contains(next)) {
        start = next;
        break;
      } else {
        curr = next;
      }
    }
  }

  if (start != nullptr) {
    Fiber* f = start;
    stderr_ << "Cycle: ";
    do {
      stderr_ << "Fiber #" << f->id << ": " << f->waiter->operation << " -> ";
      f = alive_[*(f->waiter->waiting_for)];
    } while (f != start);
    stderr_ << "Fiber #" << start->id << std::endl;
  }
}

static const size_t kReportBufSize = 4096;
static char report_buf[kReportBufSize];

void Simulator::ReportFromDeadlockedFiber(Fiber* me) {
  // User-space

  user::library::fmt::BufWriter writer{report_buf, kReportBufSize};

  writer.FormatAppend("Fiber #{} blocked at {} ({}::{} [Line {}])", me->id, me->waiter->operation,
                      me->waiter->source_loc.File(),
                      me->waiter->source_loc.Function(),
                      me->waiter->source_loc.Line());
  if (me->waiter->waiting_for) {
    writer.FormatAppend(", waiting for Fiber #{}", *(me->waiter->waiting_for));
  }
  writer.FormatAppend("\n");

  // Stack trace

  /*
  std::ostringstream trace_out;
  PrintStackTrace(trace_out);
  const auto trace = trace_out.str();
  if (!trace.empty()) {
    report_out << trace << std::endl;
  }
  */

  me->report = writer.StringView();

  me->context.SwitchTo(loop_context_);  // Never returns

  WHEELS_UNREACHABLE();
}

void Simulator::SpuriousWakeups() {
  // TODO
}

bool Simulator::DeterministicMemory() const {
#if (__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  return memory_.FixedMapping();
#else
  return false;
#endif
}

bool Simulator::DigestAtomicLoads() const {
  if (params_.digest_atomic_loads) {
    return true;  // Explicit directive
  }

  // Conservative decision
  return params_.allow_digest_atomic_loads &&
         DeterministicMemory();
}

void Breakpoint(Fiber* fiber) {
  (void)fiber;
}

void Simulator::DebugStart(Fiber* fiber) {
  Breakpoint(fiber);
}

void Simulator::DebugInterrupt(Fiber* fiber) {
  Breakpoint(fiber);
}

void Simulator::DebugSwitch(Fiber* fiber) {
  Breakpoint(fiber);
}

void Simulator::DebugResume(Fiber* fiber) {
  Breakpoint(fiber);
}

bool AmIUser() {
  return Simulator::Current()->AmIUser();
}

}  // namespace system

}  // namespace twist::rt::fiber
