#pragma once

namespace twist::rt::fiber {

namespace system {

enum class Status : int {
  Ok = 0,
  LibraryAssert,
  UnhandledException,
  Deadlock,
  MemoryLeak,
  MemoryDoubleFree,
  MemoryAccess,
  UserAbort,
  SchedulerAbort,
  Interrupted,
};

}  // namespace system

}  // namespace twist::rt::fiber