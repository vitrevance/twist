#pragma once

#include "fiber/id.hpp"

namespace twist::rt::fiber {

namespace system {

class IdAllocator {
 public:
  FiberId AllocateId() {
    return ++next_id_;
  }

  static constexpr FiberId kImpossibleId = 0;

  void Free(FiberId) {
  }

  void Reset() {
    next_id_ = 0;
  }

 private:
  uint64_t next_id_ = 0;
};

}  // namespace system

}  // namespace twist::rt::fiber
