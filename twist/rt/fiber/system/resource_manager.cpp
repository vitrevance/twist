#include "resource_manager.hpp"

namespace twist::rt::fiber {

namespace system {

void ResourceManager::Warmup() {
  for (size_t i = 0; i < 8; ++i) {
    fiber_pool_.push(new Fiber{});
  }
}

ResourceManager::~ResourceManager() {
  while (!fiber_pool_.empty()) {
    Fiber* f = fiber_pool_.top();
    fiber_pool_.pop();
    delete f;
  }
}

static thread_local ResourceManager static_manager;

ResourceManager* StaticResourceManager() {
  return &static_manager;
}

}  // namespace system

}  // namespace twist::rt::fiber
