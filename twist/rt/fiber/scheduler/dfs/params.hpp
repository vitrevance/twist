#pragma once

#include <cstdlib>
#include <optional>

namespace twist::rt::fiber {

namespace system::scheduler::dfs {

struct Params {
  std::optional<size_t> max_preempts;
  std::optional<size_t> max_steps;
  std::optional<size_t> force_preempt_after_steps;
  bool wake_order = true;
};

}  // namespace system::scheduler::dfs

}  // namespace twist::rt::fiber
