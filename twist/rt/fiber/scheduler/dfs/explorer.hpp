#pragma once

#include "schedule.hpp"

namespace twist::rt::fiber {

namespace system::scheduler::dfs {

class Explorer {
 public:
  Schedule Init() {
    return {{}};
  }

  bool Backtrack(Schedule& s) {
    while (!s.forks.empty()) {
      Fork& last = s.forks.back();
      if (last.index + 1 == last.alts) {
        s.forks.pop_back();
      } else {
        last.index++;
        return true;
      }
    }
    return false;
  }
  
 private:
};

}  // namespace system::scheduler::dfs

}  // namespace twist::rt::fiber
