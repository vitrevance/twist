#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>
#include <twist/rt/fiber/system/fiber.hpp>

#include "params.hpp"
#include "schedule.hpp"

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

#include <fmt/core.h>

// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace system::scheduler::dfs {

class Scheduler final : public IScheduler {
 public:
  using Params = dfs::Params;
  using Schedule = dfs::Schedule;

 private:
  class SortedList {
   public:
    bool IsEmpty() const {
      return size_ == 0;
    }

    size_t Size() const {
      return size_;
    }

    void Push(Fiber* f) {
      size_t i = size_++;
      while (i > 0 && list_[i - 1]->id > f->id) {
        list_[i] = list_[i - 1];
        --i;
      }
      list_[i] = f;
    }

    Fiber* Pop(size_t i) {
      Fiber* f = list_[i];
      for (size_t j = i; j + 1 < size_; ++j) {
        list_[j] = list_[j + 1];
      }
      --size_;
      return f;
    }

    Fiber* PopLast() {
      if (size_ == 0) {
        return nullptr;
      }
      return list_[--size_];
    }

    bool Remove(Fiber* f) {
      for (size_t i = 0; i < size_; ++i) {
        if (list_[i]->id == f->id) {
          Pop(i);
          return true;
        }
      }
      return false;
    }

   private:
    std::array<Fiber*, 16> list_;
    size_t size_ = 0;
  };

 private:
  // Futex queue

  class FifoWaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return queue_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return queue_.PushBack(waiter);
    }

    Fiber* Pop() override {
      return queue_.PopFront();
    }

    Fiber* PopAll() override {
      return Pop();
    }

    bool Remove(Fiber* fiber) override {
      if (fiber->IsLinked()) {
        fiber->Unlink();
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Fiber, SchedulerTag> queue_;
  };

  class ForkingWaitQueue : public IWaitQueue {
   public:
    ForkingWaitQueue(Scheduler* scheduler)
        : scheduler_(scheduler) {
    }

    bool IsEmpty() const override {
      return list_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return list_.Push(waiter);
    }

    Fiber* Pop() override {
      return scheduler_->PickWaiter(list_);
    }

    Fiber* PopAll() override {
      return list_.PopLast();  // TODO: inverse
    }

    bool Remove(Fiber* fiber) override {
      return list_.Remove(fiber);
    }

   private:
    Scheduler* scheduler_;
    SortedList list_;
  };

 public:
  Scheduler(Schedule schedule, Params params = Params())
      : params_(params), schedule_(std::move(schedule)) {
  }

  void Start(ISimContext* sim) override {
    sim_ = sim;
  }

  // System

  bool Preempt(Fiber* running, const InterruptContext*) override {
    AddStep(running);

    if (run_queue_.IsEmpty()) {
      return false;  // Alone
    }

    if (ForcedPreempt(running)) {
      if (MaxPreemptsReached()) {
        Abort(::fmt::format("Simulation blocked: force_preempt_after = {} blocked by max_preempts = {}",
                            *params_.force_preempt_after_steps,
                            *params_.max_preempts));
        WHEELS_UNREACHABLE();
      } else {
        DoPreempt(running);
        return true;  // Preempt
      }
    }

    if (MaxPreemptsReached()) {
      return false;  // Continue
    }

    // 0 - continue, 1 - preempt
    bool preempt = (Choice(2, ForkType::Preempt) == 1);

    if (preempt) {
      DoPreempt(running);
      return true;  // Preempt
    } else {
      return false;  // Continue
    }
  }

  void Yield(Fiber* running) override {
    AddStep(running);
    run_queue_.Push(running);
  }

  void Wake(Fiber* waiter) override {
    run_queue_.Push(waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_queue_.Push(fiber);
  }

  void Exit(Fiber* fiber) override {
    AddStep(fiber);
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    // (preempted != nullptr) => !run_queue_.IsEmpty()
    WHEELS_ASSERT(!run_queue_.IsEmpty() || (preempted_ == nullptr), "Broken scheduler");
    return run_queue_.IsEmpty();
  }

  Fiber* Next() override {
    Fiber* next = Pick(run_queue_, ForkType::RunQueue);
    if (preempted_) {
      run_queue_.Push(std::exchange(preempted_, nullptr));
    }
    ResetSteps(next);
    return next;
  }

  void Remove(Fiber* fiber) override {
    run_queue_.Remove(fiber);
  }

  // Run loop

  bool Maintenance() override {
    return true;
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    if (params_.wake_order) {
      return std::make_unique<ForkingWaitQueue>(this);
    } else {
      return std::make_unique<FifoWaitQueue>();
    }
  }

  // Random

  size_t RandomChoice(size_t alts) override {
    return Choice(alts, ForkType::Random);
  }

  uint64_t RandomNumber() override {
    WHEELS_PANIC("RandomNumber not supported");
  }

  bool SpuriousWakeup() override {
    return false;
  }

  // User

  bool SpuriousFailure() override {
    return false;
  }

  Schedule GetSchedule() const {
    return schedule_;
  }

 private:
  void DoPreempt(Fiber* running) {
    // NB: preempted fiber cannot be chosen on next iteration
    preempted_ = running;
    ++preempts_;
  }

  void AddStep(Fiber* running) {
    ++steps_;

    if (params_.max_steps && (steps_ > *params_.max_steps)) {
      Abort(::fmt::format("max_steps limit = {} reached", *params_.max_steps));
    }

    running->sched_context.f1 += 1;
  }

  void ResetSteps(Fiber* next) {
    next->sched_context.f1 = 0;
  }

  bool ForcedPreempt(Fiber* running) const {
    size_t steps = running->sched_context.f1;

    return params_.force_preempt_after_steps &&
           (steps >= *params_.force_preempt_after_steps);
  }

  bool MaxPreemptsReached() const {
      return params_.max_preempts && (preempts_ >= *params_.max_preempts);
  }

  Fiber* PickWaiter(SortedList& wait_queue) {
    return Pick(wait_queue, ForkType::WaitQueue);
  }

  Fiber* Pick(SortedList& list, ForkType type) {
    if (list.Size() == 1) {
      return list.Pop(0);
    }

    size_t index = Choice(list.Size(), type);
    return list.Pop(index);
  }

  size_t Choice(size_t alts, ForkType type) {
    size_t fork = forks_++;
    if (fork < schedule_.forks.size()) {
      return schedule_.forks[fork].index;
    } else {
      schedule_.forks.push_back(Fork{type, alts, 0});
      return 0;  // Go left
    }
  }

  void Abort(std::string error) {
    sim_->SchedAbort(error);
  }

 private:
  const Params params_;
  size_t forks_ = 0;
  Schedule schedule_;
  SortedList run_queue_;
  Fiber* preempted_ = nullptr;
  size_t preempts_ = 0;
  size_t steps_ = 0;
  ISimContext* sim_;
};

}  // namespace system::scheduler::dfs

}  // namespace twist::rt::fiber
