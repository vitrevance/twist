#pragma once

#include <cstdint>
#include <vector>

namespace twist::rt::fiber {

namespace system::scheduler::dfs {

enum ForkType : uint32_t {
  RunQueue,
  WaitQueue,
  Preempt,
  Random,
};

struct Fork {
  ForkType type;
  size_t alts;
  size_t index;
};

struct Schedule {
  std::vector<Fork> forks;
};

}  // namespace system::scheduler::dfs

}  // namespace twist::rt::fiber
