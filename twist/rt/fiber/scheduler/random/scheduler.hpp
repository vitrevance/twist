#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "schedule.hpp"
#include "pool.hpp"

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::random {

class Scheduler final : public IScheduler {
 public:
  using Params = random::Params;
  using Schedule = random::Schedule;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    WaitQueue(Scheduler* host)
        : host_(host) {
    }

    bool IsEmpty() const override {
      return pool_.IsEmpty();
    }

    void Push(Fiber* f) override {
      pool_.Push(f);
    }

    Fiber* Pop() override {
      return pool_.PopRandom(host_->RandomNumber());
    }

    Fiber* PopAll() override {
      return pool_.PopAll();
    }

    bool Remove(Fiber* f) override {
      return pool_.Remove(f);
    }

   private:
    Scheduler* host_;
    FiberPool pool_;
  };

 public:
  Scheduler(Schedule schedule, Params params = Params())
      : params_(params),
        random_(schedule.seed) {
  }

  void Start(ISimContext* sim) override {
    sim_ = sim;
  }

  bool Preempt(Fiber* running, const InterruptContext*) override {
    bool preempt = sim_->CurrTick() >= running->sched_context.f1;
    if (preempt) {
      run_queue_.Push(running);
    }
    return preempt;
  }

  void Yield(Fiber* running) override {
    run_queue_.Push(running);;
  }

  void Wake(Fiber* waiter) override {
    run_queue_.Push(waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_queue_.Push(fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Fiber* Next() override {
    Fiber* next = run_queue_.PopRandom(random_.Next());
    next->sched_context.f1 = sim_->CurrTick() + RandomTimeSliceInTicks();
    return next;
  }

  void Remove(Fiber* fiber) override {
    run_queue_.Remove(fiber);
  }

  // Run loop

  bool Maintenance() override {
    if (streak_ == 0) {
      streak_ = 1 + random_.Next() % params_.maintenance;
      return true;
    } else {
      --streak_;
      return false;
    }
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>(this);
  }

  // Random

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  bool SpuriousWakeup() override {
    return RandomChoice(params_.spurious) == 0;
  }

  // User

  bool SpuriousFailure() override {
    return RandomChoice(params_.spurious) == 0;
  }

 private:
  size_t RandomTimeSliceInTicks() {
    return random_.Next() % params_.time_slice;
  }

 private:
  const Params params_;
  ISimContext* sim_;
  size_t streak_ = 0;
  twist::random::WyRand random_;
  FiberPool run_queue_;
};

}  // namespace system::scheduler::random

}  // namespace twist::rt::fiber
