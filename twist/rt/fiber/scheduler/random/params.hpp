#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace system::scheduler::random {

struct Params {
  // Probability of spurious wake-up or failure is 1/{spurious}
  size_t spurious = 7;
  // Maintenance is done every RandomNumber(1, {maintenance}) iterations
  size_t maintenance = 17;
  // Fiber time slice is RandomNumber(1, {slice}) sync actions
  size_t time_slice = 10;
};

}  // namespace system::scheduler::random

}  // namespace twist::rt::fiber
