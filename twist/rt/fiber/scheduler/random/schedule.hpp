#pragma once

#include <cstdint>

namespace twist::rt::fiber {

namespace system::scheduler::random {

struct Schedule {
  // Seed for pseudo-random number generator
  uint64_t seed = 42;
};

}  // namespace system::scheduler::random

}  // namespace twist::rt::fiber
