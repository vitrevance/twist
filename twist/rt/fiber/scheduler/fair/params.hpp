#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace system::scheduler::fair {

struct Params {
  // Thread time slice (in ticks)
  size_t time_slice = 5;
};

}  // namespace system::scheduler::fair

}  // namespace twist::rt::fiber
