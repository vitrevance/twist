#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace system::scheduler::pct {

struct Params {
  size_t threads;
  size_t steps;
  size_t depth;
};

}  // namespace system::scheduler::pct

}  // namespace twist::rt::fiber
