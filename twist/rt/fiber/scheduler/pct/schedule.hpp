#pragma once

#include <cstdint>

namespace twist::rt::fiber {

namespace system::scheduler::pct {

struct Schedule {
  uint64_t seed;
};

}  // namespace system::scheduler::pct

}  // namespace twist::rt::fiber
