#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "schedule.hpp"

#include <twist/rt/fiber/system/fiber.hpp>

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

#include <fmt/core.h>

#include <vector>
#include <queue>
#include <set>

namespace twist::rt::fiber {

namespace system::scheduler::pct {

class Scheduler final : public IScheduler {
 public:
  using Params = pct::Params;
  using Schedule = pct::Schedule;

 private:
  // Run queue

  class RunQueue {
    struct Entry {
      Fiber* fiber;

      uint64_t Priority() const {
        return Scheduler::Priority(fiber);
      }

      bool operator<(const Entry& that) const {
        return Priority() < that.Priority();
      }
    };

   public:
    bool IsEmpty() const {
      return queue_.empty();
    }

    void Push(Fiber* fiber) {
      queue_.push(Entry{fiber});
    }

    Fiber* PopMaxPriority() {
      Entry top = queue_.top();
      queue_.pop();
      return top.fiber;
    }

    uint64_t MaxPriority() const {
      WHEELS_ASSERT(!IsEmpty(), "Empty run queue");
      Entry top = queue_.top();
      return top.Priority();
    }

    void Remove(Fiber*) {
      // TODO
      // Do nothing
    }

   private:
    std::priority_queue<Entry> queue_;
  };

  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return queue_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return queue_.PushBack(waiter);
    }

    Fiber* Pop() override {
      return queue_.PopFront();
    }

    Fiber* PopAll() override {
      return Pop();
    }

    bool Remove(Fiber* fiber) override {
      if (fiber->IsLinked()) {
        fiber->Unlink();
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Fiber, SchedulerTag> queue_;
  };

 public:
  Scheduler(Schedule schedule, Params params = Params())
      : params_(params), random_(schedule.seed) {
  }

  void Start(ISimContext* sim) override {
    sim_ = sim;
    Reset(params_.threads, params_.steps, params_.depth);
  }

  // System

  bool Preempt(Fiber* running, const InterruptContext*) override {
    size_t interrupt = sim_->CurrInterrupt();

    while (interrupt > change_points_[next_]) {
      ++next_;  // Skip
    }

    if (interrupt == change_points_[next_]) {
      // Change priority
      SetPriority(running, params_.depth - next_);
      ++next_;
      run_queue_.Push(running);
      return true;
    }

    if (run_queue_.IsEmpty()) {
      return false;  // Alone
    }

    if (Priority(running) < run_queue_.MaxPriority()) {
      run_queue_.Push(running);
      return true;
    }

    return false;
  }

  void Yield(Fiber*) override {
    std::abort();  // TODO
  }

  void Wake(Fiber* waiter) override {
    run_queue_.Push(waiter);
  }

  void Spawn(Fiber* fiber) override {
    SetPriority(fiber, prior_[fiber->id]);
    run_queue_.Push(fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Fiber* Next() override {
    return run_queue_.PopMaxPriority();
  }

  void Remove(Fiber* fiber) override {
    run_queue_.Remove(fiber);
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run loop

  bool Maintenance() override {
    return true;
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>();
  }

  // Random

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  bool SpuriousWakeup() override {
    return false;
  }

  // User

  bool SpuriousFailure() override {
    return false;
  }

 private:
  static uint64_t Priority(Fiber* f) {
    return f->sched_context.f1;
  }

  static void SetPriority(Fiber* f, uint64_t priority) {
    f->sched_context.f1 = priority;
  }

  void Reset(size_t threads, size_t length, size_t depth) {
    {
      // Initial priorities

      prior_.clear();

      // 1-indexed
      prior_.push_back(0);

      auto perm = RandomPermutation(threads);
      for (size_t i = 1; i <= threads; ++i) {
        prior_.push_back(depth + perm[i - 1]);
      }
    }

    {
      // Change points

      std::set<size_t> change_points;
      while (change_points.size() < depth - 1) {
        size_t step = 1 + random_.Next() % length;
        change_points.insert(step);
      }

      change_points_.clear();
      for (size_t step : change_points) {
        change_points_.push_back(step);
      }
      change_points_.push_back(std::numeric_limits<size_t>::max());

      next_ = 0;
    }
  }

  std::vector<size_t> RandomPermutation(size_t n) {
    std::vector<size_t> perm;

    perm.reserve(n);
    for (size_t i = 1; i <= n; ++i) {
      perm.push_back(i);
    }

    // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    for (size_t i = n - 1; i >= 1; --i) {
      size_t j = random_.Next() % (i + 1);
      if (i != j) {
        std::swap(perm[i], perm[j]);
      }
    }

    return perm;
  }

 private:
  const Params params_;
  std::vector<int> prior_;
  std::vector<size_t> change_points_;
  size_t next_;
  ISimContext* sim_;
  twist::random::WyRand random_{42};
  RunQueue run_queue_;
};

}  // namespace system::scheduler::pct

}  // namespace twist::rt::fiber
