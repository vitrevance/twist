#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "schedule.hpp"

#include <twist/rt/fiber/system/fiber.hpp>

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::coop {

class Scheduler final : public IScheduler {
 public:
  using Params = coop::Params;
  using Schedule = coop::Schedule;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return queue_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return queue_.PushBack(waiter);
    }

    Fiber* Pop() override {
      return queue_.PopFront();
    }

    Fiber* PopAll() override {
      return Pop();
    }

    bool Remove(Fiber* fiber) override {
      if (fiber->IsLinked()) {
        fiber->Unlink();
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Fiber, SchedulerTag> queue_;
  };

 public:
  Scheduler(Schedule schedule, Params params = Params())
      : random_(schedule.seed) {
    WHEELS_UNUSED(params);
  }

  ~Scheduler() {
    WHEELS_VERIFY(run_queue_.IsEmpty(), "Non-empty run queue");
  }

  void Start(ISimContext*) override {
    // No-op
  }

  // System

  bool Preempt(Fiber*, const InterruptContext*) override {
    return false;  // Never
  }

  void Yield(Fiber* running) override {
    run_queue_.PushBack(running);
  }

  void Wake(Fiber* waiter) override {
    run_queue_.PushBack(waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_queue_.PushBack(fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Fiber* Next() override {
    return run_queue_.PopFront();
  }

  void Remove(Fiber* fiber) override {
    if (fiber->IsLinked()) {
      fiber->Unlink();
    }
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run loop

  bool Maintenance() override {
    return true;
  }

  // Futex


  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>();
  }

  // Random

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  bool SpuriousWakeup() override {
    return false;
  }

  // User

  bool SpuriousFailure() override {
    return false;
  }

 private:
  twist::random::WyRand random_;
  wheels::IntrusiveList<Fiber, SchedulerTag> run_queue_;
};

}  // namespace system::scheduler::coop

}  // namespace twist::rt::fiber
