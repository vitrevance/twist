#pragma once

#include <twist/rt/fiber/system/tls.hpp>

#include <wheels/core/noncopyable.hpp>

#include <optional>

namespace twist::rt::fiber {

namespace user {

template <typename T>
class StaticThreadLocalVar : private system::tls::VarBase,
                             private wheels::NonCopyable {
 public:
  // NB: Do not allocate memory in ctor!
  StaticThreadLocalVar(
      const char* name,
      bool member = false,
      wheels::SourceLocation loc = wheels::SourceLocation::Current())
      : name_(name),
        loc_(loc) {
    if (member) {
      __Visit();
    }
  }

  T& operator*() {
    return *Ptr();
  }

  T* operator->() {
    return Ptr();
  }

  T* operator&() {
    return Ptr();
  }

  void operator=(T v) {
    this->operator*() = std::move(v);
  }

  void __Visit() {
    if (!key_) {
      // Global -> no more __Visit-s
      // Local -> Ctor -> key_
      system::ss::Manager::Instance().Register(this);
    }

    if (key_) {
      // Definitely local -> Maybe init if first access from current thread
      system::tls::Manager::Instance().Visit(*key_);
    }
  }

 private:
  // ss::IVar

  void Ctor() override {
    key_ = system::tls::Manager::Instance().Register(this);
  }

  void Dtor() override {
    key_.reset();
  }

  const char* Name() const override {
    return name_;
  }

  wheels::SourceLocation Loc() const override {
    return loc_;
  }

  // Isolated memory
  void Burn() override {
    key_.reset();
  }

  // tls::IVar

  void* SlotCtor() override {
    return new T{};
  }

  void SlotDtor(void* ptr) override {
    delete (T*)ptr;
  }

  //

  T* Ptr() {
    system::tls::Slot* slot = system::tls::Manager::Instance().Access(*key_);
    return slot->GetTyped<T>();
  }

 private:
  const char* name_;
  wheels::SourceLocation loc_;
  std::optional<system::tls::Key> key_;
};

}  // namespace user

}  // namespace twist::rt::fiber
