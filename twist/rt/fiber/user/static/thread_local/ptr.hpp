#pragma once

#include <twist/rt/fiber/system/ss.hpp>
#include <twist/rt/fiber/system/tls.hpp>

#include <wheels/core/noncopyable.hpp>

#include <optional>

namespace twist::rt::fiber {

namespace user {

template <typename T>
class StaticThreadLocalPtr : private system::tls::VarBase,
                             private wheels::NonCopyable {
 public:
  // NB: Do not allocate memory in ctor!

  StaticThreadLocalPtr(
      const char* name,
      T* init, wheels::SourceLocation loc = wheels::SourceLocation::Current())
      : name_(name),
        loc_(loc),
        init_(init) {
    // No-op
  }

  // Initialized with nullptr
  StaticThreadLocalPtr()
      : StaticThreadLocalPtr(nullptr) {
  }

  operator T*() {
    return Load();
  }

  StaticThreadLocalPtr& operator=(T* ptr) {
    Store(ptr);
    return *this;
  }

  // Usage: thread_local_ptr->Method();
  T* operator->() {
    return Load();
  }

  explicit operator bool() const {
    return Load() != nullptr;
  }

  void __Visit() {
    if (!key_) {
      // First visit

      // Global -> no more visits
      // Local -> Ctor -> key_
      system::ss::Manager::Instance().Register(this);
    }

    if (key_) {
      // Definitely local -> Maybe init if first access from current thread
      system::tls::Manager::Instance().Visit(*key_);
    }
  }

 private:
  // ss::IVar

  void Ctor() override {
    key_ = system::tls::Manager::Instance().Register(this);
  }

  void Dtor() override {
    key_.reset();
  }

  const char* Name() const override {
    return name_;
  }

  wheels::SourceLocation Loc() const override {
    return loc_;
  }

  // Isolated memory
  void Burn() override {
    key_.reset();
  }

  // system::tls::IVar

  void* SlotCtor() override {
    return init_;
  }

  void SlotDtor(void* /*ptr*/) override {
    // No-op
  }

  //

  T* Load() {
    return AccessSlot()->template GetTyped<T>();
  }

  void Store(T* ptr) {
    AccessSlot()->Set(ptr);
  }

  system::tls::Slot* AccessSlot() {
    return system::tls::Manager::Instance().Access(*key_);
  }

 private:
  const char* name_;
  wheels::SourceLocation loc_;
  std::optional<system::tls::Key> key_;
  T* init_;
};

}  // namespace user

}  // namespace twist::rt::fiber
