#pragma once

#include <twist/rt/fiber/system/ss.hpp>
#include <twist/rt/fiber/system/am_i_user.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/noncopyable.hpp>

namespace twist::rt::fiber {

namespace user {

void* StaticAlloc(size_t size);

template <typename T>
class StaticVar : private system::ss::VarBase,
                  private wheels::NonCopyable {
 public:
  StaticVar(
      const char* name,
      bool member = false,
      wheels::SourceLocation loc = wheels::SourceLocation::Current())
      : name_(name),
        loc_(loc) {
    if (member) {
      // Workaround for members
      __Visit();
    }
  }

  T* Ptr() {
    WHEELS_ASSERT(var_ != nullptr, "Accessing non-initialized static variable");
    return var_;
  }

  T& Ref() {
    return *Ptr();
  }

  T& operator*() {
    return Ref();
  }

  T* operator->() {
    return Ptr();
  }

  T* operator&() {
    return Ptr();
  }

  void __Visit() {
    if (var_ == nullptr) {
      // First visit

      // Global -> no more __Visit-s
      // Local -> Ctor -> var_
      system::ss::Manager::Instance().Register(this);
    }
  }

 private:
  // ss::IVar

  void Ctor() override {
    WHEELS_ASSERT(system::AmIUser(), "User thread expected");
    WHEELS_ASSERT(var_ == nullptr, "Static var already initialized");

    void* buf = StaticAlloc(sizeof(T));
    var_ = new (buf) T{};
  }

  void Dtor() override {
    WHEELS_ASSERT(system::AmIUser(), "User thread expected");
    WHEELS_ASSERT(var_ != nullptr, "Static var not initialized");
    T* var = std::exchange(var_, nullptr);
    var->~T();
  }

  const char* Name() const override {
    return name_;
  }

  wheels::SourceLocation Loc() const override {
    return loc_;
  }

  // Isolated memory
  void Burn() override {
    var_ = nullptr;
  }

 private:
  const char* name_;
  wheels::SourceLocation loc_;
  T* var_;
};

}  // namespace user

}  // namespace twist::rt::fiber
