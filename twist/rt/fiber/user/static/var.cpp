#include "var.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user {

void* StaticAlloc(size_t size) {
  return fiber::system::Simulator::Current()->UserStaticAlloc(size);
}

}  // namespace user

}  // namespace twist::rt::fiber