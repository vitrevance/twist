#pragma once

namespace twist::rt::fiber {

namespace user::scheduler {

bool SpuriousFailure();

}  // namespace user::scheduler

}  // namespace twist::rt::fiber
