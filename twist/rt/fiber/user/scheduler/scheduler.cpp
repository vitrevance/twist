#include "interrupt.hpp"
#include "spurious.hpp"
#include "preemption_guard.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user::scheduler {

void Interrupt(system::scheduler::InterruptContext* context) {
  system::Simulator::Current()->Interrupt(context);
}

bool SpuriousFailure() {
  return system::Simulator::Current()->Scheduler()->SpuriousFailure();
}

PreemptionGuard::PreemptionGuard() {
  system::Simulator::Current()->Preemptive(false);
}

PreemptionGuard::~PreemptionGuard() {
  system::Simulator::Current()->Preemptive(true);
}

}  // namespace scheduler

}  // namespace twist::rt::fiber
