#pragma once

#include <twist/rt/fiber/system/scheduler/interrupt.hpp>

#include <wheels/core/compiler.hpp>

namespace twist::rt::fiber {

namespace user::scheduler {

using Context = system::scheduler::InterruptContext;
using Placement = system::scheduler::InterruptPlacement;
using ActionType = system::scheduler::ActionType;

void Interrupt(Context* context);

inline void InterruptBefore(Context* context) {
#if defined(__TWIST_INJECT_FAULT_BEFORE__)
  context->placement = Placement::Before;
  Interrupt(context);
#else
  WHEELS_UNUSED(context);
#endif
}

inline void InterruptAfter(Context* context) {
#if defined(__TWIST_INJECT_FAULT_AFTER__)
  context->placement = Placement::After;
  Interrupt(context);
#else
  WHEELS_UNUSED(context);
#endif
}

}  // namespace scheduler

}  // namespace twist::rt::fiber
