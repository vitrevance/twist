#pragma once

namespace twist::rt::fiber {

namespace user::scheduler {

struct PreemptionGuard {
  // Disable preemption
  PreemptionGuard();

  // Non-copyable
  PreemptionGuard(const PreemptionGuard&) = delete;
  PreemptionGuard& operator=(const PreemptionGuard&) = delete;

  // Non-movable
  PreemptionGuard(PreemptionGuard&&) = delete;
  PreemptionGuard& operator=(PreemptionGuard&&) = delete;

  // Enable preemption
  ~PreemptionGuard();
};

}  // namespace user::scheduler

}  // namespace twist::rt::fiber
