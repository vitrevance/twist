#pragma once

#include <twist/rt/fiber/system/futex/key.hpp>
#include <twist/rt/fiber/system/futex/wake_count.hpp>
#include <twist/rt/fiber/system/futex/waiter.hpp>
#include <twist/rt/fiber/system/call/status.hpp>
#include <twist/rt/fiber/system/time.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::call::Status FutexWait(system::FutexKey, system::WaiterContext*);
system::call::Status FutexWaitTimed(system::FutexKey, system::Time::Instant, system::WaiterContext*);

void FutexWake(system::FutexKey, size_t);

void FutexFree(system::FutexKey);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
