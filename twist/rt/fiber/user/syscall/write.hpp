#pragma once

#include <string_view>

namespace twist::rt::fiber {

namespace user::syscall {

void Write(int fd, std::string_view buf);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
