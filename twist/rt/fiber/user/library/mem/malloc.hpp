#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

namespace user::library::mem {

void* Malloc(size_t size);
void Free(void* ptr);

}  // namespace user::library::mem

}  // namespace twist::rt::fiber
