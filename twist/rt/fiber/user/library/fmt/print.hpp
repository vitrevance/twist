#pragma once

#include "format.hpp"

#include <twist/rt/fiber/user/syscall/write.hpp>

namespace twist::rt::fiber {

namespace user::library::fmt {

template <typename... Args>
void Print(int fd, ::fmt::format_string<Args...> format_str, Args&&... args) {
  syscall::Write(fd, FormatToStaticBuf(format_str, std::forward<Args>(args)...));
}

template <typename... Args>
void Print(::fmt::format_string<Args...> format_str, Args&&... args) {
  Print(1, format_str, std::forward<Args>(args)...);
}

template <typename... Args>
void Println(int fd, ::fmt::format_string<Args...> format_str, Args&&... args) {
  syscall::Write(fd, FormatLnToStaticBuf(format_str, std::forward<Args>(args)...));
}

template <typename... Args>
void Println(::fmt::format_string<Args...> format_str, Args&&... args) {
  Println(1, format_str, std::forward<Args>(args)...);
}

}  // namespace user::library::fmt

}  // namespace twist::rt::fiber
