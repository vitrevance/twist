#include "futex.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>
#include <twist/rt/fiber/user/syscall/now.hpp>

namespace twist::rt::fiber {

namespace user::library::futex {

static system::FutexKey FutexKeyFor(std_like::Atomic<uint32_t>& atomic) {
  return (uint32_t*)&atomic;
}

void Wait(std_like::Atomic<uint32_t>& atomic, uint32_t old, std::memory_order mo,
          wheels::SourceLocation call_site) {
  system::FutexKey key = FutexKeyFor(atomic);

  while (atomic.load(mo) == old) {

    // NB: Interrupt can be injected _after_ load above
    if (atomic.__FutexWaitLoad() == old) {
      system::WaiterContext waiter{system::FutexType::Futex, "futex::Wait",
                                   call_site};
      syscall::FutexWait(key, &waiter);
    }
  }
}

bool WaitTimed(std_like::Atomic<uint32_t>& atomic, uint32_t old,
               std::chrono::milliseconds timeout, std::memory_order mo,
               wheels::SourceLocation call_site) {
  system::FutexKey key = FutexKeyFor(atomic);

  system::Time::Instant deadline = syscall::Now() + timeout;

  while (atomic.load(mo) == old) {
    if (syscall::Now() >= deadline) {
      return false;
    }
    // Ignore errors

    // NB: Interrupt can be injected _after_ load above
    if (atomic.__FutexWaitLoad() == old) {
      system::WaiterContext ctx{system::FutexType::Futex, "futex::WaitTimed",
                                call_site};
      syscall::FutexWaitTimed(key, deadline, &ctx);
    }
  }

  return true;
}

WakeKey PrepareWake(std_like::Atomic<uint32_t>& atomic) {
  return {FutexKeyFor(atomic)};
}

void WakeOne(WakeKey key) {
  syscall::FutexWake(key.futex_key, 1);
}

void WakeAll(WakeKey key) {
  syscall::FutexWake(key.futex_key, 0 /* 0 means all */);
}

}  // namespace user::library::futex

}  // namespace twist::rt::fiber
