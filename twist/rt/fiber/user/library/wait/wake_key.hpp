#pragma once

#include <twist/rt/fiber/system/futex/key.hpp>

namespace twist::rt::fiber {

namespace user::library::futex {

struct WakeKey {
  system::FutexKey futex_key;
};

}  // namespace user::library::futex

}  // namespace twist::rt::fiber
