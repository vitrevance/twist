#include <twist/rt/fiber/user/library/std_like/thread.hpp>

#include <twist/rt/fiber/system/simulator.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

#include <fmt/core.h>

// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

// ThreadId

ThreadId::ThreadId()
    : fid(system::IdAllocator::kImpossibleId) {
}

std::ostream& operator<<(std::ostream& out, ThreadId id) {
  out << ::fmt::format("{0:x}", id.fid);
  return out;
}


// Thread

static system::Fiber* const kDetached = nullptr;
static system::Fiber* const kCompleted = (system::Fiber*)1;

static system::FutexKey FutexKeyFor(Thread* thread) {
  return (system::FutexKey)thread;
}

Thread::Thread()
    : fiber_(kDetached) {
}

Thread::Thread(system::IFiberRoutine* routine, wheels::SourceLocation /*source_loc*/) {
  fiber_ = system::Simulator::Current()->SysSpawn(routine);
  fiber_->at_exit = this;
}

Thread::~Thread() {
  ___TWIST_FIBER_USER_VERIFY(IsDetached(), "Thread join or detach required");
  system::Simulator::Current()->SysFutexFree(FutexKeyFor(this));
}

Thread::Thread(Thread&& that) {
  MoveFrom(std::move(that));
}

Thread& Thread::operator=(Thread&& that) {
  ___TWIST_FIBER_USER_VERIFY(IsDetached(), "Cannot rewrite joinable thread object");
  MoveFrom(std::move(that));
  return *this;
}

void Thread::AtFiberExit() noexcept {
  fiber_ = kCompleted;
  syscall::FutexWake(ParkingLot(), 1);
}

ThreadId Thread::get_id() const noexcept {
  if (IsRunning()) {
    return ThreadId{fiber_->id};
  } else {
    return ThreadId{};
  }
}

bool Thread::joinable() const {
  return !IsDetached();  // Running or Completed
}

void Thread::detach() {
  ___TWIST_FIBER_USER_VERIFY(!IsDetached(), "Thread has already been detached");

  // Running or Completed

  if (IsRunning()) {
    // Detach
    fiber_->at_exit = nullptr;
  } else {
    // Do nothing
  }

  // Move to Detached state
  fiber_ = kDetached;
}

void Thread::join(wheels::SourceLocation call_site) {
  ___TWIST_FIBER_USER_VERIFY(!IsDetached(), "Thread has been detached");

  // Running or Completed

  system::WaiterContext waiter{system::FutexType::Thread, "thread::join", call_site};

  while (IsRunning()) {
    // Wait for completion
    waiter.waiting_for = fiber_->id;
    syscall::FutexWait(ParkingLot(), &waiter);
  }

  // Move to Detached state
  fiber_ = kDetached;
}

unsigned int Thread::hardware_concurrency() noexcept {
  return 4;
}

system::Fiber* Thread::native_handle() {
  if (IsRunning()) {
    return fiber_;
  } else {
    return nullptr;
  }
}

void Thread::swap(Thread& that) {
  std::swap(fiber_, that.fiber_);
  RebindFiber();
  that.RebindFiber();
}

bool Thread::IsDetached() const {
  return fiber_ == kDetached;
}

bool Thread::IsCompleted() const {
  return fiber_ == kCompleted;
}

bool Thread::IsRunning() const {
  return !IsDetached() && !IsCompleted();
}

void Thread::MoveFrom(Thread&& that) {
  fiber_ = std::exchange(that.fiber_, kDetached);
  RebindFiber();
}

void Thread::RebindFiber() {
  if (IsRunning()) {
    fiber_->at_exit = this;
  }
}

system::FutexKey Thread::ParkingLot() {
  return FutexKeyFor(this);
}

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
