#pragma once

#include <atomic>

namespace twist::rt::fiber {

namespace user::library::std_like {

// NOLINTNEXTLINE
inline void atomic_thread_fence(std::memory_order /*mo*/) {
  // no-op
}

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
