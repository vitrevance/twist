#pragma once

#include <cstdint>
#include <cstring>

namespace twist::rt::fiber {

namespace user::library::std_like {

class AtomicDigest {
 protected:
  template <typename T>
  void DigestLoad(T v) const {
    DigestLoadImpl(Repr(v));
  }

 private:
  template <typename T>
  uintmax_t Repr(T v) const {
    static_assert(sizeof(T) <= sizeof(uintmax_t));

    // ~ bit cast
    // TODO: endianness-invariant?
    uintmax_t repr = 0;
    std::memcpy(&repr, &v, sizeof(T));
    return repr;
  }

  void DigestLoadImpl(uintmax_t repr) const;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
