#pragma once

#include <twist/rt/fiber/user/syscall/futex.hpp>

#include <cstdlib>

namespace twist::rt::fiber {

namespace user::library::std_like {

class FutexedAtomic {
 protected:
  void AtomicWait(system::WaiterContext* waiter) {
    syscall::FutexWait(MyFutexKey(), waiter);
  }

  void AtomicNotify(size_t count) {
    syscall::FutexWake(MyFutexKey(), count);
  }

  void FreeFutex() {
    syscall::FutexFree(MyFutexKey());
  }

 private:
  system::FutexKey MyFutexKey() {
    return (system::FutexKey)this;
  }
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
