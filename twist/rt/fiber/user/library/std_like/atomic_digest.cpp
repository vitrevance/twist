#include "atomic_digest.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user::library::std_like {

void AtomicDigest::DigestLoadImpl(uintmax_t repr) const {
  if (system::Simulator* curr = system::Simulator::TryCurrent()) {
    curr->DigestAtomicLoad(repr);
  }
}

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
