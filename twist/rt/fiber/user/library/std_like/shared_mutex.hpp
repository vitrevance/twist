#pragma once

#include "futex_like.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>
#include <twist/rt/fiber/user/scheduler/spurious.hpp>

#include <cstdint>
// std::lock_guard, std::unique_lock
#include <mutex>

namespace twist::rt::fiber {

namespace user::library::std_like {

class SharedMutex {
 public:
  SharedMutex() = default;

  // Non-copyable
  SharedMutex(const SharedMutex&) = delete;
  SharedMutex& operator=(const SharedMutex&) = delete;

  // Non-movable
  SharedMutex(SharedMutex&&) = delete;
  SharedMutex& operator=(SharedMutex&&) = delete;

  // Writer

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexLock, "shared_mutex::lock"}};
    scheduler::InterruptBefore(&context);
    Lock(call_site);
    scheduler::InterruptAfter(&context);
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexUnlock, "shared_mutex::unlock"}};
    scheduler::InterruptBefore(&context);
    Unlock();
    scheduler::InterruptAfter(&context);
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexTryLock, "shared_mutex::try_lock"}};
    scheduler::InterruptBefore(&context);

    bool acquired = [this]() {
      if (scheduler::SpuriousFailure()) {
        return false;
      } else {
        return TryLock();
      }
    }();

    scheduler::InterruptAfter(&context);

    return acquired;
  }

  // Reader

  void lock_shared(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexLockShared, "shared_mutex::lock_shared"}};
    scheduler::InterruptBefore(&context);
    LockShared(call_site);
    scheduler::InterruptAfter(&context);
  }

  void unlock_shared(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexUnlockShared, "shared_mutex::unlock_shared"}};
    scheduler::InterruptBefore(&context);
    UnlockShared();
    scheduler::InterruptAfter(&context);
  }

  bool try_lock_shared(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexTryLockShared, "shared_mutex::try_lock_shared"}};
    scheduler::InterruptBefore(&context);
    bool acquired = TryLockShared();
    scheduler::InterruptAfter(&context);
    return acquired;
  }

 private:
  void Lock(wheels::SourceLocation call_site) {
    system::WaiterContext waiter{system::FutexType::MutexLock, "shared_mutex::lock", call_site};
    while (writer_ || (readers_ > 0)) {
      if (writer_) {
        waiter.waiting_for = owner_.Id();
      } else {
        waiter.waiting_for.reset();
      }
      waiters_.writers.Park(&waiter);
    }
    writer_ = true;
    owner_.Lock();
  }

  void Unlock() {
    owner_.Unlock();
    writer_ = false;

    // TODO: Readers priority? Alternate?
    if (!waiters_.writers.IsEmpty()) {
      waiters_.writers.WakeOne();
    } else {
      waiters_.readers.WakeAll();
    }
  }

  bool TryLock() {
    if (!writer_ && (readers_ == 0)) {
      writer_ = true;
      owner_.Lock();
      return true;
    } else {
      return false;
    }
  }

  // Reader

  void LockShared(wheels::SourceLocation call_site) {
    system::WaiterContext waiter{system::FutexType::MutexLock, "shared_mutex::lock_shared", call_site};
    while (writer_) {
      waiters_.readers.Park(&waiter);
    }
    ++readers_;
  }

  void UnlockShared() {
    ___TWIST_FIBER_USER_VERIFY(readers_ > 0, "The mutex must be locked by the current thread of execution in shared mode");

    if (--readers_ == 0) {
      waiters_.writers.WakeOne();
      // writers queue is empty
    }
  }

  bool TryLockShared() {
    if (!writer_) {
      ++readers_;
      return true;
    } else {
      return false;
    }
  }

 private:
  alignas(32) bool writer_{false};
  MutexOwner owner_;  // writer
  uint32_t readers_{0};

  struct Waiters {
    FutexLike writers;
    FutexLike readers;
  } waiters_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
