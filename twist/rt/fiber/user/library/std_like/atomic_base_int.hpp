#pragma once

#include "atomic_base.hpp"

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>

namespace twist::rt::fiber {

namespace user::library::std_like {

// AtomicBase for integral types

template <typename T>
class AtomicBase<T, true> : public AtomicBase<T, false> {
  using Base = AtomicBase<T, false>;

 public:
  using difference_type = T;  // NOLINT

 public:
  AtomicBase()
      : Base() {
  }

  AtomicBase(T value)
      : Base(value) {
    static_assert(sizeof(Atomic<T>) == sizeof(T));
  }

  T operator=(T new_value) {
    Base::store(new_value);
    return new_value;
  }

  // NOLINTNEXTLINE
  T fetch_add(T delta, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_add"}};
    scheduler::InterruptBefore(&context);
    T old_value = RMW([delta](T curr_value) {
      return curr_value + delta;
    });
    scheduler::InterruptAfter(&context);
    return old_value;
  }

  // NOLINTNEXTLINE
  T fetch_sub(T delta, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_sub"}};
    scheduler::InterruptBefore(&context);
    T old_value = RMW([delta](T curr_value) {
      return curr_value - delta;
    });
    scheduler::InterruptAfter(&context);
    return old_value;
  }

  // NOLINTNEXTLINE
  T fetch_and(T and_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_and"}};
    scheduler::InterruptBefore(&context);
    T old_value = RMW([and_value](T curr_value) {
      return curr_value & and_value;
    });
    scheduler::InterruptAfter(&context);
    return old_value;
  }

  // NOLINTNEXTLINE
  T fetch_or(T or_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_or"}};
    scheduler::InterruptBefore(&context);
    T old_value = RMW([or_value](T curr_value) {
      return curr_value | or_value;
    });
    scheduler::InterruptAfter(&context);
    return old_value;
  }

  // NOLINTNEXTLINE
  T fetch_xor(T xor_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_xor"}};
    scheduler::InterruptBefore(&context);
    T old_value = RMW([xor_value](T curr_value) {
      return curr_value ^ xor_value;
    });
    scheduler::InterruptAfter(&context);
    return old_value;
  }

  // Operators

  // Prefix
  T operator++() {
    return fetch_add(1) + T(1);
  }

  // Postfix
  T operator++(int) {
    return fetch_add(1);
  }

  // Prefix
  T operator--() {
    return fetch_sub(1) - T(1);
  }

  // Postfix
  T operator--(int) {
    return fetch_sub(1);
  }

  T operator+=(T delta) {
    return fetch_add(delta) + delta;
  }

  T operator-=(T delta) {
    return fetch_sub(delta) - delta;
  }

  T operator&=(T and_value) {
    return fetch_and(and_value) & and_value;
  }

  T operator|=(T or_value) {
    return fetch_or(or_value) | or_value;
  }

  T operator^=(T xor_value) {
    return fetch_xor(xor_value) ^ xor_value;
  }

 private:
  template <typename F>
  T RMW(F modify) {
    T old_value = Base::Load();   // Read
    T new_value = modify(old_value);  // Modify
    Base::Store(new_value);       // Write
    return old_value;
  }
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
