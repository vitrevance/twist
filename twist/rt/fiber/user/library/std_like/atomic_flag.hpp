#pragma once

#include "atomic_digest.hpp"
#include "atomic_futex.hpp"

#include <wheels/core/compiler.hpp>

#include <atomic>
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

class AtomicFlag
    : protected FutexedAtomic,
      protected AtomicDigest {
  using State = uint32_t;

 public:
  AtomicFlag()
      : state_(0) {
  }

  // Non-copyable
  AtomicFlag(const AtomicFlag&) = delete;
  AtomicFlag& operator=(const AtomicFlag&) = delete;

  // Non-movable
  AtomicFlag(AtomicFlag&&) = delete;
  AtomicFlag& operator=(AtomicFlag&&) = delete;

#if defined(__TWIST_ATOMIC_WAIT__)
  ~AtomicFlag() {
    FutexedAtomic::FreeFutex();
  }
#endif

  // NOLINTNEXTLINE
  void clear(std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) noexcept {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicStore, "atomic_flag::clear"}};
    scheduler::InterruptBefore(&context);
    Clear();
    scheduler::InterruptAfter(&context);
  }

  // NOLINTNEXTLINE
  bool test_and_set(std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) noexcept {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic_flag::test_and_set"}};
    scheduler::InterruptBefore(&context);
    bool r = TestAndSet();
    scheduler::InterruptAfter(&context);
    return r;
  }

  // NOLINTNEXTLINE
  bool test(std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) const noexcept {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicLoad, "atomic_flag::test"}};
    scheduler::InterruptBefore(&context);
    bool r = Test();
    scheduler::InterruptAfter(&context);
    return r;
  }

  bool DebugTest(std::memory_order mo = std::memory_order::seq_cst) const {
    WHEELS_UNUSED(mo);
    return state_ == 1;  // Do not trace
  }

#if defined(__TWIST_ATOMIC_WAIT__)

  // NOLINTNEXTLINE
  void wait(bool old, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {

    system::WaiterContext waiter{system::FutexType::Atomic, "atomic_flag::wait", call_site};
    while (test(mo) == old) {
      // TODO: Interrupt before?

      // NB: Interrupt can be injected _after_ test above
      if (WaitTest() == old) {
        FutexedAtomic::AtomicWait(&waiter);
      }
    }
  }

  // NOLINTNEXTLINE
  void notify_one(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    scheduler::Context context{{call_site, this, scheduler::ActionType::FutexWake, "atomic_flag::notify_one"}};
    scheduler::InterruptBefore(&context);

    FutexedAtomic::AtomicNotify(1);

    scheduler::InterruptAfter(&context);
  }

  // NOLINTNEXTLINE
  void notify_all(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    scheduler::Context context{{call_site, this, scheduler::ActionType::FutexWake, "atomic_flag::notify_all"}};
    scheduler::InterruptBefore(&context);

    FutexedAtomic::AtomicNotify(0);

    scheduler::InterruptAfter(&context);
  }

#endif

 private:
  bool WaitTest() const {
    return state_ == 1;
  }

  void Clear() {
    state_ = 0;
  }

  bool TestAndSet() {
    return (TraceLoad(std::exchange(state_, 1)) == 1);
  }

  bool Test() const {
    return (TraceLoad(state_) == 1);
  }

  State TraceLoad(State s) const {
    DigestLoad(s);
    return s;
  }

 private:
  // Futex support?
  State state_ = 0;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
