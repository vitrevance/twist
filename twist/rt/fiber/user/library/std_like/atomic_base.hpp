#pragma once

#include "atomic_digest.hpp"
#include "atomic_futex.hpp"

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>
#include <twist/rt/fiber/user/scheduler/spurious.hpp>

#include <wheels/core/compiler.hpp>

// std::memory_order
#include <atomic>
// std::memcmp
#include <cstring>
#include <type_traits>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

template <typename T,
          bool Int = std::is_integral_v<T> && !std::is_same_v<T, bool>>
class AtomicBase;

// AtomicBase for non-integral types

template <typename T>
class AtomicBase<T, false> : protected FutexedAtomic,
                             protected AtomicDigest {
 public:
  using value_type = T;  // NOLINT

  // https://eel.is/c++draft/atomics.types.operations#1

  [[deprecated(
      "Atomic default constructor is error-prone. Please consider explicit initialization")]] AtomicBase() noexcept(std::
                                                    is_nothrow_default_constructible_v<
                                                        T>)
      : value_(T()) {
    static_assert(std::is_default_constructible<T>());
  }

  AtomicBase(T value)
      : value_(value) {
  }

  // Non-copyable
  AtomicBase(const AtomicBase<T>&) = delete;
  AtomicBase<T>& operator=(const AtomicBase<T>&) = delete;

  // Non-movable
  AtomicBase(AtomicBase<T>&&) = delete;
  AtomicBase<T>& operator=(AtomicBase<T>&&) = delete;

  ~AtomicBase() {
    if constexpr (FutexWaitable()) {
      FutexedAtomic::FreeFutex();
    }
  }

  bool is_lock_free() const noexcept {
    return true;
  }

  static constexpr bool is_always_lock_free = true;

  // NOLINTNEXTLINE
  T load(std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) const {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicLoad, "atomic::load"}};
    scheduler::InterruptBefore(&context);
    T v = Load();
    scheduler::InterruptAfter(&context);
    return v;
  }

  // Hide from IDE
  T __FutexWaitLoad() const {
    return value_;
  }

  T DebugLoad(std::memory_order mo = std::memory_order::seq_cst) const {
    WHEELS_UNUSED(mo);
    return value_;  // Do not trace
  }

  operator T() const {
    return load();
  }

  // NOLINTNEXTLINE
  void store(T new_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicStore, "atomic::store"}};
    scheduler::InterruptBefore(&context);

    Store(new_value);

    scheduler::InterruptAfter(&context);
  }

  // NOLINTNEXTLINE
  T exchange(T new_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::exchange"}};

    scheduler::InterruptBefore(&context);

    T old = Exchange(new_value);

    scheduler::InterruptAfter(&context);

    return old;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order success = std::memory_order::seq_cst,
      std::memory_order failure = std::memory_order::seq_cst,
      wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(success);
    WHEELS_UNUSED(failure);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::compare_exchange_strong"}};
    scheduler::InterruptBefore(&context);
    bool committed = CompareExchange(expected, desired);
    scheduler::InterruptAfter(&context);
    return committed;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order success = std::memory_order::seq_cst,
      std::memory_order failure = std::memory_order::seq_cst,
      wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(success);
    WHEELS_UNUSED(failure);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::compare_exchange_weak"}};
    scheduler::InterruptBefore(&context);

    bool committed;
    if (scheduler::SpuriousFailure()) {
      committed = false;
    } else {
      committed = CompareExchange(expected, desired);
    }

    scheduler::InterruptAfter(&context);

    return committed;
  }

  // wait / notify

#if defined(__TWIST_ATOMIC_WAIT__)

  // NOLINTNEXTLINE
  void wait(T old, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    while (BitwiseEqual(load(mo), old)) {
      // Interrupt can be injected _after_ load above
      if (BitwiseEqual(__FutexWaitLoad(), old)) {
        system::WaiterContext waiter{system::FutexType::Atomic, "atomic::wait",
                                     call_site};
        FutexedAtomic::AtomicWait(&waiter);
      }
    }
  }

  // NOLINTNEXTLINE
  void notify_one(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    scheduler::Context context{{call_site, this, scheduler::ActionType::FutexWake, "atomic::notify_one"}};
    scheduler::InterruptBefore(&context);

    FutexedAtomic::AtomicNotify(1);

    scheduler::InterruptAfter(&context);
  }

  // NOLINTNEXTLINE
  void notify_all(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    scheduler::Context context{{call_site, this, scheduler::ActionType::FutexWake, "atomic::notify_all"}};
    scheduler::InterruptBefore(&context);

    FutexedAtomic::AtomicNotify(system::FutexWakeCount::All);

    scheduler::InterruptAfter(&context);
  }

#endif

 private:
  T Exchange(T new_value) {
    T old_value = Load();
    Store(new_value);
    return old_value;
  }

  bool CompareExchange(T& expected, T desired) {
    T curr = Load();

    if (BitwiseEqual(curr, expected)) {
      Store(desired);
      return true;
    } else {
      expected = curr;
      return false;
    }
  }

 protected:
  T Load() const {
    return TraceLoad(value_);
  }

  void Store(T v) {
    value_ = v;
  }

 private:
  static constexpr bool FutexWaitable() {
#if defined(__TWIST_ATOMIC_WAIT__)
    return true;
#else
    return std::is_same_v<T, uint32_t>;
#endif
  }

  static bool BitwiseEqual(const T& lhs, const T& rhs) {
    return std::memcmp(&lhs, &rhs, sizeof(T)) == 0;
  }

  T TraceLoad(T v) const {
    DigestLoad(v);
    return v;
  }

 private:
  T value_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
