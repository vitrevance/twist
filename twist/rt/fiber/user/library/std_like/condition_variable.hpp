#pragma once

#include "futex_like.hpp"

#include <twist/rt/fiber/user/scheduler/preemption_guard.hpp>
#include <twist/rt/fiber/user/scheduler/interrupt.hpp>

#include <twist/rt/fiber/user/library/std_like/chrono.hpp>

// std::unique_lock
#include <mutex>
// std::cv_status
#include <condition_variable>

namespace twist::rt::fiber {

namespace user::library::std_like {

class CondVar {
 public:
  CondVar() = default;

  // Non-copyable
  CondVar(const CondVar&) = delete;
  CondVar& operator=(const CondVar&) = delete;

  // Non-movable
  CondVar(CondVar&&) = delete;
  CondVar& operator=(CondVar&&) = delete;

  // std::condition_variable interface

  template <typename Lock>
  void wait(Lock& lock, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::CondVarWait, "condition_variable::wait"}};
    scheduler::InterruptBefore(&context);
    Wait(lock, call_site);
    scheduler::InterruptAfter(&context);
  }

  template <typename Lock>
  std::cv_status wait_until(Lock& lock, chrono::Clock::time_point expiration_time, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::CondVarWait, "condition_variable::wait_until"}};
    scheduler::InterruptBefore(&context);

    system::WaiterContext waiter{system::FutexType::CondVar, "condition_variable::wait_until", call_site};
    auto status = WaitTimed(lock, expiration_time, &waiter);

    scheduler::InterruptAfter(&context);

    return status;
  }

  template <typename Lock>
  std::cv_status wait_for(Lock& lock, chrono::Clock::duration timeout, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::CondVarWait, "condition_variable::wait_for"}};
    scheduler::InterruptBefore(&context);

    system::WaiterContext waiter{system::FutexType::CondVar, "condition_variable::wait_for", call_site};
    auto status = WaitTimed(lock, chrono::Clock::now() + timeout, &waiter);

    scheduler::InterruptAfter(&context);

    return status;
  }

  template <typename Lock, typename Predicate>
  void wait(Lock& lock, Predicate predicate, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (!predicate()) {
      wait(lock, call_site);
    }
  }

  void notify_one(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::CondVarWake, "condition_variable::notify_one"}};
    scheduler::InterruptBefore(&context);
    NotifyOne();
    scheduler::InterruptAfter(&context);
  }

  void notify_all(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::CondVarWake, "condition_variable;:notify_all"}};
    scheduler::InterruptBefore(&context);
    NotifyAll();
    scheduler::InterruptAfter(&context);
  }

 private:
  template <typename Lock>
  void Wait(Lock& lock, wheels::SourceLocation call_site) {
    {
      scheduler::PreemptionGuard guard;
      lock.unlock();
    }
    system::WaiterContext waiter{system::FutexType::CondVar, "condition_variable::wait", call_site};
    waiters_.Park(&waiter);
    lock.lock();
  }

  template <typename Lock>
  std::cv_status WaitTimed(Lock& lock, system::Time::Instant expiration_time, system::WaiterContext* waiter) {
    {
      scheduler::PreemptionGuard guard;
      lock.unlock();
    }
    auto status = waiters_.ParkTimed(expiration_time, waiter);
    lock.lock();

    if (status == system::call::Status::Timeout) {
      return std::cv_status::timeout;
    } else {
      // Maybe spurious wakeup
      return std::cv_status::no_timeout;
    }
  }

  void NotifyOne() {
    waiters_.WakeOne();
  }

  void NotifyAll() {
    waiters_.WakeAll();
  }

 private:
  FutexLike waiters_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
