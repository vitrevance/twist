#pragma once

#include "futex_like.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/library/std_like/chrono.hpp>
#include <twist/rt/fiber/user/library/std_like/chrono/deadline.hpp>

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>
#include <twist/rt/fiber/user/scheduler/spurious.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

class TimedMutex {
 public:
  TimedMutex() = default;

  // Non-copyable
  TimedMutex(const TimedMutex&) = delete;
  TimedMutex& operator=(const TimedMutex&) = delete;

  // Non-movable
  TimedMutex(TimedMutex&&) = delete;
  TimedMutex& operator=(TimedMutex&&) = delete;

  // std::mutex / Lockable

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexLock, "timed_mutex::lock"}};
    scheduler::InterruptBefore(&context);
    Lock(call_site);
    scheduler::InterruptAfter(&context);
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexTryLock, "timed_mutex::try_lock"}};
    scheduler::InterruptBefore(&context);

    bool acquired = [this]() {
      if (scheduler::SpuriousFailure()) {
        return false;
      } else {
        return TryLock();
      }
    }();

    scheduler::InterruptAfter(&context);

    return acquired;
  }

  bool try_lock_for(chrono::Clock::duration timeout, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexTryLock, "timed_mutex::try_lock_for"}};
    scheduler::InterruptBefore(&context);

    bool acquired = [this, timeout, call_site]() {
      if (scheduler::SpuriousFailure()) {
        return false;
      } else {
        system::WaiterContext waiter{system::FutexType::MutexTryLock, "timed_mutex::try_lock_for", call_site};
        return TryLockTimed(DeadLine::FromTimeout(timeout), &waiter);
      }
    }();

    scheduler::InterruptAfter(&context);

    return acquired;
  }

  // NB: steady_clock and system_clock are both aliases of the same Clock,
  // so no template over clock type is required

  bool try_lock_until(chrono::Clock::time_point d, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexTryLock, "timed_mutex::try_lock_until"}};
    scheduler::InterruptBefore(&context);

    bool acquired = [this, d, call_site]() {
      if (scheduler::SpuriousFailure()) {
        return false;
      } else {
        system::WaiterContext waiter{system::FutexType::MutexTryLock, "timed_mutex::try_lock_until", call_site};
        return TryLockTimed(DeadLine{d}, &waiter);
      }
    }();

    scheduler::InterruptAfter(&context);

    return acquired;
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexUnlock, "timed_mutex::unlock"}};
    scheduler::InterruptBefore(&context);
    Unlock();
    scheduler::InterruptAfter(&context);
  }

 private:
  void Lock(wheels::SourceLocation call_site) {
    if (!locked_) {
      system::WaiterContext waiter{system::FutexType::MutexLock,
                                   "timed_mutex::lock", call_site};
      do {
        waiter.waiting_for = owner_.Id();
        waiters_.Park(&waiter);
      } while (locked_);
    }
    locked_ = true;
    owner_.Lock();
  }

  bool TryLock() {
    if (!locked_) {
      locked_ = true;
      owner_.Lock();
      return true;
    } else {
      return false;
    }
  }

  bool TryLockTimed(DeadLine d, system::WaiterContext* waiter) {
    while (locked_) {
      if (d.Expired()) {
        return false;
      }
      waiter->waiting_for = owner_.Id();
      waiters_.ParkTimed(d.TimePoint(), waiter);
    }
    locked_ = true;
    owner_.Lock();
    return true;
  }

  void Unlock() {
    ___TWIST_FIBER_USER_VERIFY(locked_, "Unlocking mutex that is not locked");
    owner_.Unlock();
    locked_ = false;
    waiters_.WakeOne();
  }

 private:
  bool locked_{false};
  MutexOwner owner_;
  FutexLike waiters_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
