#pragma once

#include <twist/rt/fiber/user/library/std_like/atomic_t.hpp>

// std::ptrdiff_t
#include <cstddef>

namespace twist::rt::fiber {

namespace user::library::std_like {

template <typename T>
class Atomic<T*> : public AtomicBase<T*, false> {
  using Base = AtomicBase<T*, false>;

 public:
  using difference_type = std::ptrdiff_t;  // NOLINT

 public:
  Atomic()
      : Base() {
  }

  Atomic(T* ptr)
      : Base(ptr) {
    static_assert(sizeof(Atomic<T*>) == sizeof(T*));
  }

  T* operator=(T* new_ptr) {
    Base::store(new_ptr);
    return new_ptr;
  }

  // NOLINTNEXTLINE
  T* fetch_add(std::ptrdiff_t delta, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_add"}};
    scheduler::InterruptBefore(&context);
    T* old_ptr = FetchAdd(delta);
    scheduler::InterruptAfter(&context);
    return old_ptr;
  }

  // NOLINTNEXTLINE
  T* fetch_sub(std::ptrdiff_t delta, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(mo);

    scheduler::Context context{{call_site, this, scheduler::ActionType::AtomicRMW, "atomic::fetch_sub"}};
    scheduler::InterruptBefore(&context);
    T* old_ptr = FetchAdd(-delta);
    scheduler::InterruptAfter(&context);
    return old_ptr;
  }

  // Operators

  // Prefix
  T* operator++() {
    return fetch_add(1) + 1;
  }

  // Postfix
  T* operator++(int) {
    return fetch_add(1);
  }

  // Prefix
  T* operator--() {
    return fetch_sub(1) - 1;
  }

  // Postfix
  T* operator--(int) {
    return fetch_sub(1);
  }

  T* operator+=(std::ptrdiff_t delta) {
    return fetch_add(delta) + delta;
  }

  T* operator-=(std::ptrdiff_t delta) {
    return fetch_sub(delta) - delta;
  }

 private:
  T* FetchAdd(std::ptrdiff_t delta) {
    T* old_ptr = Base::Load();
    Base::Store(old_ptr + delta);
    return old_ptr;
  }
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
