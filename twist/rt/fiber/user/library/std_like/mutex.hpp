#pragma once

#include "futex_like.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/scheduler/interrupt.hpp>
#include <twist/rt/fiber/user/scheduler/spurious.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

class Mutex {
 public:
  Mutex() = default;

  // Non-copyable
  Mutex(const Mutex&) = delete;
  Mutex& operator=(const Mutex&) = delete;

  // Non-movable
  Mutex(Mutex&&) = delete;
  Mutex& operator=(Mutex&&) = delete;

  // std::mutex / Lockable

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexLock, "mutex::lock"}};
    scheduler::InterruptBefore(&context);
    Lock(call_site);
    scheduler::InterruptAfter(&context);
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexTryLock, "mutex::try_lock"}};
    scheduler::InterruptBefore(&context);

    bool acquired = [this]() {
      if (scheduler::SpuriousFailure()) {
        return false;
      } else {
        return TryLock();
      }
    }();

    scheduler::InterruptAfter(&context);

    return acquired;
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    scheduler::Context context{{call_site, this, scheduler::ActionType::MutexUnlock, "mutex::unlock"}};
    scheduler::InterruptBefore(&context);
    Unlock();
    scheduler::InterruptAfter(&context);
  }

 private:
  void Lock(wheels::SourceLocation call_site) {
    if (locked_) {
      system::WaiterContext waiter{system::FutexType::MutexLock, "mutex::lock",
                                   call_site};
      do {
        waiter.waiting_for = owner_.Id();
        waiters_.Park(&waiter);
      } while (locked_);
    }
    locked_ = true;
    owner_.Lock();
  }

  bool TryLock() {
    if (!locked_) {
      locked_ = true;
      owner_.Lock();
      return true;
    } else {
      return false;
    }
  }

  void Unlock() {
    ___TWIST_FIBER_USER_VERIFY(locked_, "Unlocking mutex that is not locked");
    owner_.Unlock();
    locked_ = false;
    waiters_.WakeOne();
  }

 private:
  bool locked_{false};
  MutexOwner owner_;
  FutexLike waiters_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
