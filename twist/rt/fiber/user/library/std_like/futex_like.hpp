#pragma once

#include <twist/rt/fiber/user/syscall/futex.hpp>

namespace twist::rt::fiber {

namespace user::library::std_like {

class FutexLike {
 public:
  void Park(system::WaiterContext* waiter) {
    ++size_;
    syscall::FutexWait(Key(), waiter);
    --size_;
  }

  system::call::Status ParkTimed(system::Time::Instant d, system::WaiterContext* waiter) {
    ++size_;
    auto status = syscall::FutexWaitTimed(Key(), d, waiter);
    --size_;

    return status;
  }

  bool IsEmpty() const {
    return size_ == 0;
  }

  void WakeOne() {
    if (size_ > 0) {
      syscall::FutexWake(Key(), 1);
    }
  }

  void WakeAll() {
    if (size_ > 0) {
      syscall::FutexWake(Key(), system::FutexWakeCount::All);
    }
  }

  ~FutexLike() {
    syscall::FutexFree(Key());
  }

 private:
  system::FutexKey Key() const {
    static_assert(sizeof(FutexLike) >= 4);
    return (system::FutexKey)this;
  }

 private:
  // User-space counter
  uint64_t size_ = 0;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
