#pragma once

#include <twist/rt/fiber/user/syscall/random.hpp>

#include <wheels/core/assert.hpp>

#include <cstdlib>

namespace twist::rt::fiber {

namespace user::library::random {

inline size_t Choose(size_t alts) {
  WHEELS_ASSERT(alts > 0, "Positive number of alternatives expected");
  return syscall::RandomChoice(alts);
}

inline bool Either() {
  return Choose(2) == 0;
}

}  // namespace user::library::library::random

}  // namespace twist::rt::fiber
