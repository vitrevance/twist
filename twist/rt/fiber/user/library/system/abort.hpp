#pragma once

#include <twist/rt/fiber/user/syscall/abort.hpp>

namespace twist::rt::fiber {

namespace user::library::system {

[[noreturn]] void Abort() {
  syscall::Abort(fiber::system::Status::UserAbort);
}

}  // namespace user::library::system

}  // namespace twist::rt::fiber
