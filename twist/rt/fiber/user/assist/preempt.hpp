#pragma once

#include "../scheduler/interrupt.hpp"

namespace twist::rt::fiber {

namespace user::assist {

inline void PreemptionPoint(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  scheduler::Context context{{call_site, nullptr, scheduler::ActionType::Local, "PreemptionPoint"}};
  scheduler::Interrupt(&context);
}

}  // namespace user::assist

}  // namespace twist::rt::fiber
