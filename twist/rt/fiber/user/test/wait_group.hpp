#pragma once

#include <twist/rt/fiber/system/fiber/routine.hpp>
#include <twist/rt/fiber/system/fiber/at_exit.hpp>

#include <twist/rt/fiber/user/library/std_like/atomic.hpp>

#include <wheels/core/source_location.hpp>

namespace twist::rt::fiber {

namespace user::test {

class WaitGroup : private system::IFiberExitHandler {
 private:
  struct UserRoutineNode : system::IFiberRoutine {
    UserRoutineNode* next;
  };

  template <typename F>
  struct UserRoutine : UserRoutineNode {
    explicit UserRoutine(F&& f)
        : fn(std::move(f)) {
    }

    void RunUser() override {
      fn();
    }

    F fn;
    UserRoutine* next;
  };

 public:
  WaitGroup() {
  }

  template <typename F>
  WaitGroup& Add(F fn) {
    UserRoutineNode* r = new UserRoutine{std::move(fn)};
    r->next = head_;
    head_ = r;

    return *this;
  }

  template <typename F>
  WaitGroup& Add(size_t count, F fn) {
    for (size_t i = 0; i < count; ++i) {
      Add(fn);
    }

    return *this;
  }

  void Join(wheels::SourceLocation call_site = wheels::SourceLocation::Current());

 private:
  void AtFiberExit() noexcept override;

 private:
  UserRoutineNode* head_ = nullptr;
  library::std_like::Atomic<size_t> count_{0};
};

}  // namespace user::test

}  // namespace twist::rt::fiber
