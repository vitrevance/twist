#include "wait_group.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/rt/fiber/user/scheduler/preemption_guard.hpp>

namespace twist::rt::fiber {

namespace user::test {

static system::FutexKey WaitKeyFor(WaitGroup* wg) {
  return (system::FutexKey)wg;
}

void WaitGroup::Join(wheels::SourceLocation call_site) {
  // Spawn fibers

  size_t count = 0;

  UserRoutineNode* r = head_;
  while (r != nullptr) {
    system::Fiber* f = system::Simulator::Current()->SysSpawn(r);
    f->at_exit = this;
    ++count;
    r = r->next;
  }

  {
    scheduler::PreemptionGuard guard;
    count_.store(count);
  }

  {
    system::WaiterContext waiter{system::FutexType::WaitGroup,
                                 "WaitGroup::Join", call_site};
    system::Simulator::Current()->SysFutexWait(WaitKeyFor(this), &waiter);
  }
}

void WaitGroup::AtFiberExit() noexcept {
  bool done = [this] {
    scheduler::PreemptionGuard guard;
    return count_.fetch_sub(1) == 1;
  }();

  if (done) {
    system::Simulator::Current()->SysFutexWake(WaitKeyFor(this), 1 /*~all*/);
  }
}

}  // namespace user::test

}  // namespace twist::rt::fiber
