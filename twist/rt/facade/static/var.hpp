#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/static/var.hpp>
#include <twist/rt/fiber/user/static/visit.hpp>

#define TWISTED_STATIC_VAR(T, name) \
  static twist::rt::fiber::user::StaticVar<T> name{#name}; \
  ___TWIST_VISIT_STATIC_VAR(name)

#else

#include <twist/rt/thread/static/var.hpp>

#define TWISTED_STATIC_VAR(T, name) static twist::rt::thread::StaticVar<T> name{}

#endif
