#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/random/choice.hpp>

namespace twist::rt::facade {

namespace random {

using fiber::user::library::random::Choose;
using fiber::user::library::random::Either;

}  // namespace random

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/random/choice.hpp>

namespace twist::rt::facade {

namespace random {

using thread::random::Choose;
using thread::random::Either;

}  // namespace random

}  // namespace twist::rt::facade

#endif
