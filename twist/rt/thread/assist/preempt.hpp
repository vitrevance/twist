#pragma once

namespace twist::rt::thread::assist {

inline void PreemptionPoint() {
  // No-op
}

}  // namespace twist::rt::thread::assist
