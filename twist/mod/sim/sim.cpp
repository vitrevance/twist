#include "det.hpp"

#include "sched/coop.hpp"
#include "sched/fair.hpp"
#include "sched/random.hpp"
#include "sched/pct.hpp"
#include "sched/dfs.hpp"

#include <twist/rt/fiber/scheduler/dfs/explorer.hpp>

#include <random>

namespace twist::sim {

//

Digest DetCheckSim(SimulatorParams params, MainRoutine main) {
  {
    /*
     * /\ Isolated user memory
     * /\ Same memory mapping for same process
     */
    params.digest_atomic_loads = true;
    // Do not crash in determinism check
    params.crash_on_abort = false;
    params.forward_stdout = false;
  }

  static const size_t kIters = 512;

  random::Schedule schedule{42};
  random::Scheduler scheduler{schedule, {}};

  Simulator simulator{&scheduler, params};

  simulator.Silent(true);
  simulator.Start(main);
  simulator.RunFor(kIters);
  auto ret = simulator.Burn();

  return ret.digest;
}

bool DetCheck(SimulatorParams params, MainRoutine main) {
  Digest d1 = DetCheckSim(params, main);
  Digest d2 = DetCheckSim(params, main);

  if (d1 != d2) {
    return false;
  }

  Digest d3 = DetCheckSim(params, main);

  if (d3 != d1) {
    return false;
  }

  return true;
}

//

namespace coop {

Result Run(RunParams params, Schedule schedule, MainRoutine main) {
  Scheduler scheduler{schedule, params.coop};
  Simulator simulator{&scheduler, params.sim};

  return simulator.Run(main);
}

}  // namespace coop

//

namespace fair {

Result Run(RunParams params, Schedule schedule, MainRoutine main) {
  Scheduler scheduler{schedule, params.fair};
  Simulator simulator{&scheduler, params.sim};

  return simulator.Run(main);
}

}  // namespace fair

//

namespace random {

Result Run(Params params, Schedule schedule, MainRoutine main) {
  Scheduler scheduler{schedule, params.random};
  Simulator simulator{&scheduler, params.sim};

  return simulator.Run(main);
}

ExploreResult Explore(Params params, MainRoutine main, size_t limit) {
  {
    // Determinism check
    bool det = DetCheck(params.sim, main);
    if (!det) {
      wheels::Panic("Simulation is not deterministic");
    }
  }

  {
    // Simulator parameters
    params.sim.forward_stdout = false;
    params.sim.crash_on_abort = false;
    params.sim.randomize_malloc = false;
  }

  std::mt19937_64 twister;

  for (size_t i = 1; i <= limit; ++i) {
    Schedule schedule{.seed = twister()};
    auto result = Run(params, schedule, main);
    if (!result.Ok()) {
      return ExploreResult{
        .found ={{schedule, result}},
        .sims=i,
      };
    }
  }

  return ExploreResult{{}, limit};
}

}  // namespace random

//

namespace pct {

Result Run(Params params, Schedule schedule, MainRoutine main) {
  Scheduler scheduler{schedule, params.pct};
  Simulator simulator{&scheduler, params.sim};
  return simulator.Run(main);
}

ExploreResult Explore(Params params, MainRoutine main, size_t limit) {
  {
    // Determinism check
    bool det = DetCheck(params.sim, main);
    if (!det) {
      wheels::Panic("Simulation is not deterministic");
    }
  }

  {
    // Simulator parameters
    params.sim.forward_stdout = false;
    params.sim.crash_on_abort = false;
    params.sim.randomize_malloc = false;
  }

  size_t threads = 0;
  size_t length = 0;

  {
    // Estimate number of threads and steps with Random scheduler
    auto random_params = random::Params{};
    random_params.sim = params.sim;

    auto random_schedule = random::Schedule{.seed=42};

    auto result = random::Run(random_params, random_schedule, main);

    threads = result.threads;
    length = result.interrupts;
  }

  std::mt19937_64 twister;

  for (size_t i = 1; i <= limit; ++i) {
    params.pct.threads = threads;
    params.pct.steps = length;

    Schedule schedule{.seed = twister()};

    Scheduler scheduler{schedule, params.pct};
    Simulator simulator{&scheduler, params.sim};

    auto result = simulator.Run(main);

    if (!result.Ok()) {
      return {
        .found ={{schedule,result}},
        .sims=i,
      };
    }

    length = std::max(length, result.interrupts);
  }

  return {{}, limit};
}

}  // namespace pct

//

namespace dfs {

Result Run(Params params, Schedule schedule, MainRoutine main) {
  Scheduler scheduler{schedule, params.dfs};
  Simulator sim{&scheduler, params.sim};

  return sim.Run([main] { main(); });
}

ExploreResult Explore(Params params, MainRoutine main) {
  {
    bool det = DetCheck(params.sim, main);
    if (!det) {
      wheels::Panic("Simulation is not deterministic");
    }
  }

  {
    params.sim.forward_stdout = false;
    params.sim.crash_on_abort = false;
    params.sim.randomize_malloc = false;
  }

  twist::rt::fiber::system::scheduler::dfs::Explorer explorer;
  auto s = explorer.Init();

  size_t count = 0;

  do {
    Scheduler scheduler{s, params.dfs};
    Simulator sim{&scheduler, params.sim};

    auto result = sim.Run([main] { main(); });
    ++count;

    s = scheduler.GetSchedule();

    if (!result.Ok() && result.status != Status::SchedulerAbort) {
      return ExploreResult{
        .found ={{s,result}},
        .sims=count,
      };
    }
  } while (explorer.Backtrack(s));

  return {{}, count};
}

}  // namespace dfs

}  // namespace twist::sim
