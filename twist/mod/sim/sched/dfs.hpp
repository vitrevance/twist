#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/dfs/scheduler.hpp>

#include <optional>

namespace twist::sim {

namespace dfs {

using twist::rt::fiber::system::scheduler::dfs::Scheduler;
using twist::rt::fiber::system::scheduler::dfs::Schedule;
using SchedulerParams = twist::rt::fiber::system::scheduler::dfs::Params;

struct Params {
  SimulatorParams sim;
  SchedulerParams dfs;
};

Result Run(Params, Schedule, MainRoutine);

struct Found {
  Schedule schedule;
  Result result;
};

struct ExploreResult {
  std::optional<Found> found;
  size_t sims;
};

ExploreResult Explore(Params, MainRoutine);

}  // namespace dfs

}  // namespace twist::sim
