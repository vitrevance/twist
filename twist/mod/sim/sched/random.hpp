#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/random/scheduler.hpp>

#include <optional>

namespace twist::sim {

namespace random {

using twist::rt::fiber::system::scheduler::random::Scheduler;
using twist::rt::fiber::system::scheduler::random::Schedule;
using SchedulerParams = twist::rt::fiber::system::scheduler::random::Params;

struct Params {
  SimulatorParams sim;
  SchedulerParams random;
};

Result Run(Params, Schedule, MainRoutine);

inline Result Run(Params params, MainRoutine main) {
  return Run(params, {.seed=42}, main);
}

struct Found {
  Schedule schedule;
  Result result;
};

struct ExploreResult {
  std::optional<Found> found;
  size_t sims;
};

ExploreResult Explore(Params, MainRoutine, size_t limit);

}  // namespace random

}  // namespace twist::sim
