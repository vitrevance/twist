#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/pct/scheduler.hpp>

#include <optional>

namespace twist::sim {

namespace pct {

using twist::rt::fiber::system::scheduler::pct::Scheduler;
using twist::rt::fiber::system::scheduler::pct::Schedule;
using SchedulerParams = twist::rt::fiber::system::scheduler::pct::Params;

struct Params {
  SimulatorParams sim;
  SchedulerParams pct;
};

Result Run(Params, Schedule, MainRoutine);

struct Found {
  Schedule schedule;
  Result result;
};

struct ExploreResult {
  std::optional<Found> found;
  size_t sims;
};

ExploreResult Explore(Params, MainRoutine, size_t limit);

}  // namespace pct

}  // namespace twist::sim
