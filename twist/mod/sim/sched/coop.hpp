#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/coop/scheduler.hpp>

namespace twist::sim {

namespace coop {

using twist::rt::fiber::system::scheduler::coop::Scheduler;
using twist::rt::fiber::system::scheduler::coop::Schedule;
using SchedulerParams = twist::rt::fiber::system::scheduler::coop::Params;

struct RunParams {
  SimulatorParams sim;
  SchedulerParams coop;
};

Result Run(RunParams, Schedule, MainRoutine);

// Defaults
inline Result Run(MainRoutine main) {
  return Run({}, {.seed=42}, std::move(main));
}

}  // namespace coop

}  // namespace twist::sim
