#pragma once

#include "../simulator.hpp"

#include <twist/rt/fiber/scheduler/fair/scheduler.hpp>

namespace twist::sim {

namespace fair {

using twist::rt::fiber::system::scheduler::fair::Scheduler;
using twist::rt::fiber::system::scheduler::fair::Schedule;
using SchedulerParams = twist::rt::fiber::system::scheduler::fair::Params;

struct RunParams {
  SimulatorParams sim;
  SchedulerParams fair;
};

Result Run(RunParams, Schedule, MainRoutine);

// Defaults
inline Result Run(MainRoutine main) {
  return Run({}, {}, std::move(main));
}

}  // namespace fair

}  // namespace twist::sim
